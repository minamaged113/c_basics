/**
 * @file main.c
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief Hackerrank problem:
 * https://www.hackerrank.com/challenges/printing-pattern-2/problem?isFullScreen=true
 * @version 1.0
 * @date 2024-06-01
 *
 * @copyright Copyright (c) Mina Ghobrial 2024 under MIT LICENSE
 *
 */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils/utils.h"

void logic(const int *n);

int main(void) {
    int n;
    scanf("%d", &n);
    logic(&n);
    return 0;
}

/**
 * @brief Print numerical patterns.
 *
 * Generate a pattern of numbers from 1 to n
 * Example Output for n = 4
 *      4 4 4 4 4 4 4
 *      4 3 3 3 3 3 4
 *      4 3 2 2 2 3 4
 *      4 3 2 1 2 3 4
 *      4 3 2 2 2 3 4
 *      4 3 3 3 3 3 4
 *      4 4 4 4 4 4 4
 *
 * Algorithm explained:
 *
 *  - Input and Matrix Size Calculation:
 *      - Read the integer n.
 *      - Calculate the size of the matrix as length = 2 * n - 1.
 *
 *  - Nested Loops for matrix filling:
 *      - Use two nested loops: one for the rows (rows) and one for the columns
 *        (cols).
 *
 *  - Determine Value for Each Position:
 *      - For each position (i, j) in the matrix, calculate the minimum distance
 *        to any edge of the matrix. The value to be printed at each position is
 *        n - min_dist, where min_dist is the minimum of the distances to the
 *        four edges.
 *
 *  - Print the matrix:
 *      - Print each value followed by a space.
 *      - Print a newline character after each row.
 *
 * @param n The main number for the pattern.
 */
void logic(const int *n) {
    int length = (*n * 2) - 1;
    int min_dist;
    double _intermediate_var;

    for (int row = 0; row < length; row++) {
        for (int col = 0; col < length; col++) {

            _intermediate_var =
                Min(4, (double)row, (double)col, (double)(length - 1 - row),
                    (double)(length - 1 - col));
            min_dist = (int)_intermediate_var;
            printf("%d ", *n - min_dist);
        }
        printf("\n");
    }
}
