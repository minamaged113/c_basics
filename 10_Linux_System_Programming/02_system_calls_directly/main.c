/**
 * @file main.c
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief Sample program that uses system calls directly
 * @version 0.1
 * @date 2022-12-16
 *
 * @copyright Copyright (c) Mina Ghobrial 2022 under MIT LICENSE
 *
 */

#include <string.h>
#include <unistd.h>
#define CHAR_BUFF_LEN 100
#define CURRENT_STDOUT 1 // (screen)

int main(void) {
    unsigned long msg_len = 0;
    char buf[CHAR_BUFF_LEN] = "This is writing to screen using write() system "
                              "call instead of printf Library call\n";

    msg_len = strlen(buf);

    (void)write(CURRENT_STDOUT, buf, msg_len);
    return 0;
}
