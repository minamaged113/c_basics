/**
 * @file main.c
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief Sample program
 *
 * Sample program to show usage of standard library
 * stdio.h is the standard Input/Output header file
 *
 * @version 0.1
 * @date 2022-12-16
 *
 * @copyright Copyright (c) Mina Ghobrial 2022 under MIT LICENSE
 *
 */

#include "utils/utils.h"

/**
 * @brief Print a statement to the screen using library call
 *
 * @return int
 */
int main(void) {
    (void)printf("\n This is a demo to show how to call a library function\n");
    return 0;
}
