# Pointer Decay

In C, not all array names are treated as pointers to the array. Pointer decay is
when the array name points to the 1st element in the array only and not the
whole array.

## References

- https://www.youtube.com/watch?v=WL1P6xiA_KY
- https://www.geeksforgeeks.org/what-is-array-decay-in-c-how-can-it-be-prevented/
- https://www.geeksforgeeks.org/using-sizof-operator-with-array-paratmeters-in-c/
- https://stackoverflow.com/questions/1641957/is-an-array-name-a-pointer
- https://stackoverflow.com/questions/55545441/c-what-is-array-name-when-not-converted-to-pointer-of-its-type
- https://stackoverflow.com/questions/33291624/why-do-arrays-in-c-decay-to-pointers
- https://stackoverflow.com/questions/72704674/c-sizeof-arrays-in-function-decleration
- https://jameshfisher.com/2016/12/08/c-array-decaying/
