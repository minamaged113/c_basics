/**
 * @file main.c
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief
 * @version 0.1
 * @date 2023-03-27
 *
 * @copyright Copyright (c) Mina Ghobrial 2023 under MIT LICENSE
 *
 */

#include "utils/utils.h"

int main(void) {
    // Declare an array of integers of size 5
    int arr[5];
    // Declare another as 2D with dimensions 3 and 5
    int matrix[3][5] = {{0, 1, 2, 3, 4}, {5, 6, 7, 8, 9}, {10, 11, 12, 13, 14}};

    // The name `arr` refers to an array of size 5
    // using the size of, it should print 5 * sizeof(int)
    // in most systems an int is 4 bytes, so it prints 20.
    printf("        arr size: %lu\n", sizeof(arr));

    // Array pointer decay
    // If you refer to an array by directly specifying its name, C creates a
    // pointer to the first element in that array.
    // This is passing an array by value.
    // The value is the address of the first element
    printf("             arr: %llu\n", (unsigned long long)arr);
    printf("           arr+1: %llu\n", (unsigned long long)(arr + 1));
    // arr -> pointer to the first element in the array
    // arr + 1 -> points to the second element in the array (increments arr
    //            pointer value by sizeof(int))
    //                                    ^ type of array elements

    printf("\n");
    // Passing an array by reference
    // The array is passed by reference, using the & operator. The passes the
    // whole address space of the array with the starting value of the first
    // element address.
    printf("            &arr: %llu\n", (unsigned long long)(&arr));
    printf("          &arr+1: %llu\n", (unsigned long long)(&arr + 1));
    // &arr -> pointer to the entire array.
    // &arr + 1 -> pointer to the 1st address after the array. (increments by
    //             5 * sizeof(int) = sizeof(arr))
    //             ^          ^ type of array elements
    //             ^ number of elements in array

    printf("\n");

    // The consequences of the previously explained behavior appears more when
    // dealing with n-D arrays.
    // Consider the 2D array "matrix[3][5]"

    // The following line gives a pointer to the 1st element [0] in the 2nd
    // row[1] -> Address of matrix[1][0]
    printf("       matrix[1]: %llu\n", (unsigned long long)(matrix[1]));
    // Next line should give the address of the 2nd element [1] in the 2nd row
    // [1] -> Address of matrix[1][1]
    printf("     matrix[1]+1: %llu\n", (unsigned long long)(matrix[1] + 1));
    // dereferencing
    printf("  *(matrix[1]+1): %llu\n", (unsigned long long)*(matrix[1] + 1));

    printf("\n");

    // The following line gives a pointer to the 2nd row -> matrix[1][*]
    printf("      &matrix[1]: %llu\n", (unsigned long long)(&matrix[1]));
    // Next line gives pointer to 3rd row -> matrix[2][*]
    printf("    &matrix[1]+1: %llu\n", (unsigned long long)(&matrix[1] + 1));
    // Dereferencing: gives address of 1st element [0] in 2nd row [1]
    printf(" *(&matrix[1]+1): %llu\n", (unsigned long long)*(&matrix[1] + 1));
    // Dereferencing again gives value
    printf("**(&matrix[1]+1): %llu\n", (unsigned long long)**(&matrix[1] + 1));

    return EXIT_SUCCESS;
}
