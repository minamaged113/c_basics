################################################################################
# Set test properties
set(TEST_NAME "qa_clang_format")

set(TEST_COMMAND "${C_BASICS_TOOLS_DIR}/clang_format.sh")
set(TEST_ARGS "")
list(APPEND TEST_ARGS -w)
list(APPEND TEST_ARGS ${CMAKE_SOURCE_DIR}) # workspace dir
list(APPEND TEST_ARGS -b)
list(APPEND TEST_ARGS ${CMAKE_BINARY_DIR}) # build dir

set(TEST_LABELS "quality")

################################################################################
# Add test and its properties
add_test(
    NAME ${TEST_NAME}
    COMMAND ${TEST_COMMAND} ${TEST_ARGS}
    WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
)

set_property(
    TEST ${TEST_NAME}
    PROPERTY
    LABELS ${TEST_LABELS}
)
