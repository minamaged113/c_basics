# Credits for https://stackoverflow.com/a/46337096/6081035
# LICENSE: Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0)

function(print_target_properties tgt)
    if(NOT TARGET ${tgt})
        message("There is no target named '${tgt}'")
        return()
    endif()

    # this list of properties can be extended as needed
    set(CMAKE_PROPERTY_LIST SOURCE_DIR BINARY_DIR COMPILE_DEFINITIONS
             COMPILE_OPTIONS INCLUDE_DIRECTORIES LINK_LIBRARIES)

    message(STATUS "++++++ ${tgt}")
    message(STATUS "INFO | Configuration for target ${tgt}")

    foreach (prop ${CMAKE_PROPERTY_LIST})
        get_property(propval TARGET ${tgt} PROPERTY ${prop} SET)
        if (propval)
            get_target_property(propval ${tgt} ${prop})
            message (STATUS "INFO | ${prop} = ${propval}")
        endif()
    endforeach(prop)
endfunction(print_target_properties)