#!/usr/bin/env bash

TextContents_CmakelistsTextContent() {
    # $1: Project name without index
    # $2: Project name with index
    printf "cmake_minimum_required(VERSION \${C_BASICS_CMAKE_GLOBAL_VERSION})

################################################################################
# Create a static library that includes the program functions.
set(TARGET_LIB \"$1\")

add_library(\${TARGET_LIB}
STATIC
    \${CMAKE_CURRENT_SOURCE_DIR}/src/mainlib.c
)

target_include_directories(\${TARGET_LIB}
PRIVATE
    \${CMAKE_CURRENT_SOURCE_DIR}/include
    \${C_BASICS_UTILS_INCLUDES}
)

target_link_libraries(\${TARGET_LIB}
PUBLIC
    \${C_BASICS_UTILS_LIBRARY}
)
################################################################################
# Creating main target
set(TARGET \"$(printf $2 | cut -d '_' -f 1)_\${TARGET_LIB}\")
add_executable(\${TARGET}
    \${CMAKE_CURRENT_SOURCE_DIR}/main.c
)

target_include_directories(\${TARGET}
PUBLIC
    \${CMAKE_CURRENT_SOURCE_DIR}/include
)

target_link_libraries(\${TARGET}
PUBLIC
    \${TARGET_LIB}
    \${C_BASICS_UTILS_LIBRARY}
)

print_target_properties(\${TARGET})

################################################################################
# Creating main target tests
set(TARGET_TEST \"test_\${TARGET}\")
add_test(NAME \${TARGET_TEST} COMMAND \${TARGET_TEST})

add_executable(\${TARGET_TEST}
    \${CMAKE_CURRENT_SOURCE_DIR}/tests/test_mainlib.cpp
)

target_include_directories(\${TARGET_TEST}
PUBLIC
    \${CMAKE_CURRENT_SOURCE_DIR}/include
)

target_link_libraries(\${TARGET_TEST}
PUBLIC
    gtest
    pthread
    \${TARGET_LIB}
)

set_target_properties(\${TARGET_TEST} PROPERTIES COMPILE_OPTIONS \"\")
"
}

TextContents_ReadmeContent() {
    # $1: Raw project name
    printf "# $1\n"
}

TextContents_MainExecutableContent() {
    # $1: Project name
    # $2: Project brief description
    printf "/**
 * @file main.c
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief $2
 * @version 0.1
 * @date $(date +%Y-%m-%d)
 *
 * @copyright Copyright (c) Mina Ghobrial $(date +%Y) under MIT LICENSE
 *
 */

#include \"$1/mainlib.h\"

int main() {
    return EXIT_SUCCESS;
}
"
}

TextContents_MainlibExecutableContent() {
    # $1: Raw project name
    # $2: Project name
    printf "/**
 * @file mainlib.c
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief $1 function definitions.
 * @version 0.1
 * @date $(date +%Y-%m-%d)
 *
 * @copyright Copyright (c) Mina Ghobrial $(date +%Y) under MIT LICENSE
 *
 */

#include \"$2/mainlib.h\"

void Run() {
    return;
}

"
}

TextContents_MainlibHeaderContent() {
    # $1: Raw project name
    # $2: Project name
    local PROJECT_NAME_UPPERCASE=$(printf "$2" | tr '[:lower:]' '[:upper:]')
    printf "/**
 * @file mainlib.h
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief $1 function declarations.
 * @version 0.1
 * @date $(date +%Y-%m-%d)
 *
 * @copyright Copyright (c) Mina Ghobrial $(date +%Y) under MIT LICENSE
 *
 */

#if !defined("$PROJECT_NAME_UPPERCASE"_MAINLIB_H)
#define "$PROJECT_NAME_UPPERCASE"_MAINLIB_H

#include \"utils/utils.h\"

void Run(void);

#endif // "$PROJECT_NAME_UPPERCASE"_MAINLIB_H
"
}

TextContents_TestMainlibContent() {
    # $1: Raw project name
    # $2: Project name
    printf "/**
 * @file test_mainlib.cpp
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief $1 function tests.
 * @version 0.1
 * @date $(date +%Y-%m-%d)
 *
 * @copyright Copyright (c) Mina Ghobrial $(date +%Y) under MIT LICENSE
 *
 */

#include <gtest/gtest.h>

// prevent C++ name mangling to allow linking to C code
// https://www.geeksforgeeks.org/extern-c-in-c/
#ifdef __cplusplus
extern \"C\" {
#endif
#include \"$2/mainlib.h\"
#ifdef __cplusplus
}
#endif

TEST($2, DUMMY_TEST) { EXPECT_EQ(1, 1); }

int main(int argc, char *argv[]) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
"
}
