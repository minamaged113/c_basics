#!/usr/bin/env bash
################################################################################
set -e
VALID_ARGS=$(getopt -o b:w: --long build:workspace: -- "$@")
if [[ $? -ne 0 ]]; then
    exit 1;
fi

eval set -- "$VALID_ARGS"
while [ : ]; do
  case "$1" in
    -b | --build)
        BUILD_DIR=$2
        shift 2
        ;;
    -w | --workspace)
        WORKSPACE=$2
        shift 2
        ;;
    --) shift;
        break
        ;;
  esac
done

################################################################################
# Get all source files
sources=$(find ${WORKSPACE} -name '**.c' | grep -v ${BUILD_DIR} | tr '\n' ' ')

################################################################################
# Analyze all files
clang-tidy -header-filter='.*' -p ${BUILD_DIR} $sources
