#!/usr/bin/env bash

Common_GetGitBaseDir() {
    # show $GIT_DIR if defined else show path to .git directory
    local dirname=$(git rev-parse --git-dir)

    if [ "$dirname" = ".git" ]; then
        printf "$PWD"
    else
        printf "$dirname"
    fi
}

Common_GetNumberOfProjects() {
    local BASE_DIR=$(Common_GetGitBaseDir)
    printf "$(ls -a $BASE_DIR | tr ' ' '\n' | grep -E '[0-9]' | cut -d '_' -f 1 | tail -n 1)"
}

Common_AddProjectToMainCmakelists() {
    # $1: Full project name
    local ROOT_CMAKEFILE="$(Common_GetGitBaseDir)/CMakeLists.txt"
    local LAST_SUBDIRECTORY_ENTRY=$(grep -r 'add_subdirectory' $ROOT_CMAKEFILE | tail -n 1)
    sed -i "s/$LAST_SUBDIRECTORY_ENTRY/$LAST_SUBDIRECTORY_ENTRY\nadd_subdirectory($1)/g" $ROOT_CMAKEFILE
}

LogDebug() {
    # To enable logging by this function, append `DEBUG=1` when running your
    # scripts.
    # Example:
    #   DEBUG=1 ./path/to/script.sh
    if [[ $DEBUG == 1 ]]; then
        printf "$(date +%Y:%d:%m:%X) | DEBUG | "
        printf "$1"
        printf "\n"
    fi
}
