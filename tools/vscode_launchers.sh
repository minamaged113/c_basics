#!/usr/bin/env bash
################################################################################
set -e
VALID_ARGS=$(getopt -o b:w: --long build:workspace: -- "$@")
if [[ $? -ne 0 ]]; then
    exit 1;
fi

eval set -- "$VALID_ARGS"
while [ : ]; do
  case "$1" in
    -b | --build)
        BUILD_DIR=$2
        shift 2
        ;;
    -w | --workspace)
        WORKSPACE=$2
        shift 2
        ;;
    --) shift;
        break
        ;;
  esac
done

################################################################################
# Get list of targets
targets=$(find ${BUILD_DIR} -name '[0-9]*' -type f | rev | cut -d '/' -f 1 | rev)

# VS Code Launchers
launchers=$(grep '"program":' ${WORKSPACE}/.vscode/launch.json | tr -s ' ' | cut -d '/' -f 3- | cut -d '"' -f 1 | xargs)

################################################################################
# task-1: Check all targets exist
for launcher in $launchers
do # loop over launchers
    realpath "${BUILD_DIR}/$launcher" 1>/dev/null
done

################################################################################
# task-2: Check if every target has a launcher
for target in $targets
do # loop over targets
    for launcher in $launchers
    do # loop over launchers
        target_match=false
        target_in_launcher=$(echo $launcher | cut -d '/' -f 2)
        # if target matches target name from launchers
        if [[ $target == $target_in_launcher ]]; then
            target_match=true
            break
        fi
    done
    if [[ $target_match = false ]]; then
        echo "ERROR: Target - ${target} not found."
        exit 1
    fi
done
