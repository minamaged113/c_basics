/**
 * @file server.c
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief Implement the server functionality.
 * @version 0.1
 * @date 2022-09-23
 *
 * @copyright Copyright (c) Mina Ghobrial 2022 under MIT LICENSE
 *
 */

// Custom includes
#include "utils/utils.h"

// for htons
#include <arpa/inet.h>

// handle signals
#include <signal.h>

// Common includes
#include <stdio.h>
#include <stdlib.h>

// Sockets: man sockets
#include <sys/socket.h>
#include <sys/types.h>

// Descriptor functions (read and close)
#include <unistd.h>

static volatile sig_atomic_t stop;

static void HandleServerSigint(int signal) {
    // Signal handler has to take an int input
    (void)printf("Received signal: %d.", signal);
    // set stop variable to exit while loop and stop execution
    stop = 1;
}

static void ActivateLogic(int16_t val) {
    (void)printf("Value received: %d\n", val);
}

int main(void) {
    int server_descriptor = 0;
    int client_descriptor = 0;
    char *strtol_ptr = NULL;
    int16_t integer_value_received;
    struct sockaddr_in address;
    socklen_t socklen;

    stop = 0;
    signal(SIGINT, (sig_t)HandleServerSigint);
    (void)printf("=== SERVER ===\n");

    // Construct the server
    address.sin_family = AF_INET; // Socket is IPv4
    address.sin_addr.s_addr = inet_addr(LOCAL_HOST_IP_ADDR);
    address.sin_port = htons(kPort);

    (void)printf("running on: http://%s:%u/\n", inet_ntoa(address.sin_addr),
                 address.sin_port);

    // Create TCP socket, IPv4
    server_descriptor = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    // check if socket created successfully
    if (server_descriptor < 0) {
        perror("ERROR: Server failed to create a socket");
        return EXIT_FAILURE;
    }

    // Binding a name to a socket
    if (bind(server_descriptor, (struct sockaddr *)&address, sizeof(address)) <
        0) {
        perror("ERROR: Server failed to bind with socket");
        close(server_descriptor);
        return EXIT_FAILURE;
    }

    // Listen to incoming traffic
    if (listen(server_descriptor, kMaxConnections) < 0) {
        perror("ERROR: Server encountered listening error");
        close(server_descriptor);
        return EXIT_FAILURE;
    }

    // Wait and Accept connections
    (void)printf("Waiting and accepting connections...\n");
    socklen = sizeof(address);
    client_descriptor =
        accept(server_descriptor, (struct sockaddr *)&address, &socklen);

    if (client_descriptor < 0) {
        perror("ERROR: Server accept error");
        close(server_descriptor);
        return EXIT_FAILURE;
    }

    // Client information
    (void)printf("Client with IP %s connected\n", inet_ntoa(address.sin_addr));

    // Receive the following data
    while (!stop) {
        // Recreate allocation to clear the buffer
        char *msg = (char *)calloc(kWrtiableDataSize, sizeof(char));

        // read from what client wrote in file descriptor
        if (read(client_descriptor, msg, kWrtiableDataSize) <= 0) {
            // if connection closed
            stop = 1;
        } else {
            (void)printf("SERVER - Received %s\t", msg);
            integer_value_received = (int16_t)strtol(msg, &strtol_ptr, kBase10);
            ActivateLogic(integer_value_received);
        }
        // free message resources
        free(msg);
    }

    // Cleanup and exit
    close(server_descriptor);
    return EXIT_SUCCESS;
}
