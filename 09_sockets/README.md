# Server-Client communication using Sockets

The client will generate sine wave values to emulate an analog sensor sine wave.

The sine wave will have amplitudes from 0 to 255.

The server should interpret any value higher than 127 as an activation value.
This means that Server will not react on values from 0 to 127.
From 128 to 255, the server will activate its logic.

## Start

```bash
./server    # start the server
./client    # then start the client
```

## Stop

Stop the client and the server will stop automatically.

## References

1. https://riptutorial.com/posix/example/22276/socket-basics
1. https://www.youtube.com/watch?v=cbPwYFP92D4&list=PLuhsSjG-BPPKC3Mom3rvNnTOCELLYRxj0&index=15