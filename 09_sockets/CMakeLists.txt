cmake_minimum_required(VERSION ${C_BASICS_CMAKE_GLOBAL_VERSION})

################################################################################
# SERVER
add_executable(09_server server.c)

target_link_libraries(09_server
PUBLIC ${C_BASICS_UTILS_LIBRARY}
)

print_target_properties(09_server)

################################################################################
# CLIENT
add_executable(09_client client.c)

target_link_libraries(09_client
PUBLIC
    ${C_BASICS_UTILS_LIBRARY}
    m   
)

print_target_properties(09_client)
