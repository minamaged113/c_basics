/**
 * @file client.c
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief Client functionality.
 * @version 0.1
 * @date 2022-09-24
 *
 * @copyright Copyright (c) Mina Ghobrial 2022 under MIT LICENSE
 *
 */
// Custom includes
#include "utils/utils.h"

// for htons
#include <arpa/inet.h>

#include <math.h>

// handle signals
#include <signal.h>

// For sockets
#include <sys/socket.h>
#include <sys/types.h>

// Common includes
#include <stdio.h>
#include <stdlib.h>

#include <unistd.h> // for sleep & descriptor functions

#define AMPLITUDE 40
#define US_TO_MS 1000
#define SLEEP_INTERVAL_US 50

static volatile sig_atomic_t stop;

static void HandleServerSigint(int signal) {
    // Signal handler has to take an int input
    (void)printf("Received signal: %d.", signal);
    // set stop variable to exit while loop and stop execution
    stop = 1;
}

static void Plot(int8_t val) {
    char string[2 * AMPLITUDE + 2] = {0};
    for (size_t i = 0; i < 2 * AMPLITUDE + 1; i++) {
        string[i] = ' ';
    }

    string[AMPLITUDE] = '|';
    // string[2*AMPLITUDE+1] = '\0';
    /**
     * The current array looks something like this:
     * "..........|.........."
     * "-ve values|+ve values"
     *  |         |         |
     *  V         ⋁         V
     *  0       AMPLITUDE   2*AMPLITUDE+1
     *
     * if val is < 0, fill the left half
     */
    if (val < 0) {
        for (int i = (AMPLITUDE - 1); i >= (AMPLITUDE + val); i--) {
            // start in the middle and fill the left half
            string[i] = '=';
        }

    } else if (val > 0) {
        // start from the middle and fill the right half
        for (int i = (AMPLITUDE + 1); i <= (AMPLITUDE + val); i++) {
            string[i] = '=';
        }
    }
    (void)printf("%s\r", string);
}

int main(void) {
    // Socket client
    int client_descriptor = 0;
    struct sockaddr_in address;

    double sensor_reading = 0;
    double frequency = FREQUENCY; // frequency
    double shift = 0;             // phase shift
    double time = 0;              // time

    stop = 0;

    signal(SIGINT, (sig_t)HandleServerSigint);
    (void)printf("=== CLIENT ===\n");

    // Create the IPv4 config of the client
    address.sin_family = AF_INET; // IPv4
    address.sin_port = htons(kPort);
    address.sin_addr.s_addr = inet_addr(LOCAL_HOST_IP_ADDR);
    (void)printf("running on: http://%s:%u/\n", inet_ntoa(address.sin_addr),
                 address.sin_port);

    // Create the client socket
    client_descriptor = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (client_descriptor < 0) {
        perror("CLIENT - ERROR: Client failed to create a socket");
        return EXIT_FAILURE;
    }

    // Connect to server
    if (connect(client_descriptor, (struct sockaddr *)&address,
                sizeof(address)) < 0) {
        perror("CLIENT - ERROR: Client failed to connect to server");
        close(client_descriptor);
        return EXIT_FAILURE;
    }

    // Connected to server
    (void)printf("CLIENT - Connected to server");

    // Start reading and sending sensor information
    // loop until stop signal received
    while (!stop) {
        // Create buffer to send message
        char *msg = (char *)calloc(kWrtiableDataSize, sizeof(char));

        // emulate sensor reading as a sine wave
        sensor_reading = AMPLITUDE * sin(2 * M_PI * frequency * time + shift);

        // Plot the value
        Plot((int8_t)sensor_reading);

        // Prepare data for sending
        /**
         * DEVIATION:
         *
         * IGNORE: Call to function 'snprintf' is insecure as it does not
         * provide security checks introduced in the C11 standard. Replace with
         * analogous functions that support length arguments or provides
         * boundary checks such as 'snprintf_s' in case of C11
         * [clang-analyzer-security.insecureAPI.DeprecatedOrUnsafeBufferHandling,-warnings-as-errors]
         *
         * JUSTIFICATION: 'snprintf_s' only part of Bounds-checking interfaces
         * (Annex K). Proposal to remove such feature submitted in:
         * https://www.open-std.org/jtc1/sc22/wg14/www/docs/n1969.htm#tc
         *
         * REFERENCES: More information can be found:
         * - https://www.open-std.org/jtc1/sc22/wg14/www/docs/n1570.pdf
         *
         */
        // NOLINTNEXTLINE : https://rules.sonarsource.com/c/RSPEC-6069
        (void)snprintf(msg, kWrtiableDataSize, "%d", (int8_t)sensor_reading);
        // sprintf_s();

        // send data
        write(client_descriptor, msg, kWrtiableDataSize);

        // Prepare for next iteration
        time++;
        usleep(SLEEP_INTERVAL_US * US_TO_MS);
        fflush(stdout);

        // free message resources
        free(msg);
    }

    // Cleanup and exit
    close(client_descriptor);
    return EXIT_SUCCESS;
}
