# ASCII Graphics

Generate an ASCII triangle of a given character. The triangle is generated in 2
shapes. The first shape is half a triangle, the other is a full triangle.

In the sample run below, the triangle is formed with the character `#` and
spaces are filled with `-` character.

## Sample Run

```bash
./program 
Height: 8       # input number of rows
-------#
------##
-----###
----####
---#####
--######
-#######
########
-------#--#-------
------##--##------
-----###--###-----
----####--####----
---#####--#####---
--######--######--
-#######--#######-
########--########
```
