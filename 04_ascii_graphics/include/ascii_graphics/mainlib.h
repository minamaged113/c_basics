/**
 * @file mainlib.h
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief Declarations of ASCII Graphics functions.
 * @version 0.1
 * @date 2023-02-10
 *
 * @copyright Copyright (c) Mina Ghobrial 2023 under MIT LICENSE
 *
 */
#if !defined(ASCII_GRAPHICS_MAINLIB_H)
#define ASCII_GRAPHICS_MAINLIB_H

#include "utils/utils.h"

#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#define MAX_CHARS 9

/**
 * @brief Get the user input for triangle height value between 1 and 8.
 *
 * @return int Height of the triangle to be drawn.
 */
int GetUserInput(void);

/**
 * @brief Get the width of the line to draw. The formula to calculate
 *  the width is w = (height * repeat) + offset
 *
 * @param height Height of the triangle.
 * @param offset If mirrored, this is the offset between both.
 * @param duplicate `true` if mirrored. `false` otherwise.
 * @return unsigned char Width of the triangle. This is also the number of
 *  columns to be drawn.
 */
unsigned char GetWidth(unsigned char height, unsigned char offset,
                       bool duplicate);

/**
 * @brief Get the drawable line. Computes the current row to be printed.
 *
 * @param line A pointer to an array of characters to be edited.
 *  SIDE EFFECT: This array is changed during execution.
 * @param height Height of the triangle to be drawn.
 * @param row_number Row number of the current line in the triangle shape.
 * @param block A character to print as the main building block of the
 *  triangle.
 * @param empty_block A character to print as the secondary building block
 *  of the triangle.
 */
void GetDrawableLine(unsigned char *line, int height, int row_number,
                     unsigned char block, unsigned char empty_block);

/**
 * @brief Mirros the given array of characters `line` over a given offset.
 *
 * @param line A pointer to an array of characters to be edited.
 *  SIDE EFFECT: This array is changed during execution.
 * @param height Height of the triangle to be drawn.
 * @param MAX_WIDTH Width of the triangle after mirroring including the
 *  offset.
 * @param offset Number of characters between the original array and the mirror.
 * @param offset_char The character to print as offset.
 */
void MirrorLine(unsigned char *line, unsigned char height,
                unsigned char MAX_WIDTH, unsigned char offset,
                unsigned char offset_char);

/**
 * @brief Draw two triangles, where one is mirror of the other.
 *
 * @param block The main building character of the triangle.
 * @param empty_block The secondary building character of the triangle.
 * @param height Height of the 2 triangles.
 */
void DrawMirroredTriangle(unsigned char block, unsigned char empty_block,
                          unsigned char height);

/**
 * @brief Draw a triangle.
 *
 * @param block The main building character of the triangle.
 * @param empty_block The secondary building character of the triangle.
 * @param height Height of the 2 triangles.
 */
void DrawTriangle(unsigned char block, unsigned char empty_block,
                  unsigned char height);

#endif // ASCII_GRAPHICS_MAINLIB_H
