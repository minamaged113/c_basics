#ifndef ASCII_GRAPHICS_TRIANGLE_LINKED_LIST_H
#define ASCII_GRAPHICS_TRIANGLE_LINKED_LIST_H

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * @brief A node in a doubly linked list.
 *
 * This structure represents a node in a doubly linked list. It contains:
 * - `character`: A character data member.
 * - `next`: A pointer to the next node in the list.
 * - `prev`: A pointer to the previous node in the list.
 */
typedef struct Node {
    unsigned char character;
    struct Node *next;
    struct Node *prev;
} Node_t;

/**
 * @brief Initializes a new doubly linked list.
 *
 * Allocates memory for the head node and initializes its `next` and `prev`
 * pointers to NULL.
 *
 * @return A pointer to the newly created head node.
 */
Node_t *InitList(void);

/**
 * @brief Deletes all nodes in the linked list.
 *
 * Iterates through the list, freeing each node's memory.
 *
 * @param head A pointer to the head node of the list.
 * @return 0 on success.
 */
int DeleteList(Node_t *head);

/**
 * @brief Adds a new node to the beginning of the linked list.
 *
 * Allocates memory for a new node, sets its data, and updates the head and next
 * pointers.
 *
 * @param value The character value to be stored in the new node.
 * @param HEAD A pointer to the head pointer of the list.
 */
void PushFront(unsigned char value, Node_t **HEAD);

/**
 * @brief Deletes nodes with the given value from the linked list.
 *
 * Iterates through the list, searching for nodes with the specified value. If
 * found, removes nodes and returns their character value.
 *
 * @param value The character value to be deleted.
 * @param HEAD A pointer to the head pointer of the list.
 * @return The character value of the deleted node, or 0 if not found.
 */
unsigned char DeleteByValue(unsigned char value, Node_t *HEAD);

/**
 * @brief Prints the characters in the linked list.
 *
 * Iterates through the list and prints the character of each node.
 *
 * @param HEAD A pointer to the head node of the list.
 */
void PrintList(Node_t *HEAD);

/**
 * @brief Sets the character of a node at a given index.
 *
 * Iterates through the list to find the node at the specified index and updates
 * its character.
 *
 * @param nodeIndex The index of the node to modify.
 * @param value The new character value.
 * @param HEAD A pointer to the head node of the list.
 */
void SetNodeCharacter(size_t nodeIndex, unsigned char value, Node_t *HEAD);

/**
 * @brief Draws a triangle pattern using a linked list.
 *
 * This function creates a linked list, populates it with characters, and prints
 * the triangle pattern to the console.
 *
 * @param block The character to use for the triangle's blocks.
 * @param empty_block The character to use for empty spaces.
 * @param height The height of the triangle.
 * @return 0 on success, non-zero on failure.
 */
int DrawTriangleLinkedList(unsigned char block, unsigned char empty_block,
                           size_t height);

#endif // ASCII_GRAPHICS_TRIANGLE_LINKED_LIST_H
