/**
 * @file mainlib.c
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief Implementation of ASCII Graphics functions.
 * @version 0.1
 * @date 2023-02-10
 *
 * @copyright Copyright (c) Mina Ghobrial 2023 under MIT LICENSE
 *
 */
#include "ascii_graphics/mainlib.h"

int GetUserInput(void) {
    // number of rows & columns
    int height = 0;
    char c_input[BUFFER_SIZE] = {0};
    char *ptr = NULL;
    do {
        // prompt the user for unsigned int between 1 and 8
        printf("Height: ");
        if (fgets(c_input, BUFFER_SIZE, stdin) != NULL) {
            height = (int)strtol(c_input, &ptr, kBase10);
        }
    } while (height <= 0 | height > MAX_CHARS - 1);
    return height;
}

unsigned char GetWidth(unsigned char height, unsigned char offset,
                       bool duplicate) {
    unsigned char repeat = duplicate ? 2 : 1;
    return (height * repeat) + offset;
}

static void FillWithEmptyBlockChar(unsigned char *inp_line, int height,
                                   int row_number,
                                   const unsigned char empty_block) {
    // loop over given line to fill array with spaces
    for (int spaces = height - row_number - 2; spaces >= 0; spaces--) {
        inp_line[spaces] = empty_block;
    }
}

static void FillWithBlockChar(unsigned char *inp_line, int height,
                              int row_number, const unsigned char block) {
    for (int hashes = height - row_number - 2; hashes < height - 1; hashes++) {
        inp_line[hashes + 1] = block;
    }
}

void GetDrawableLine(unsigned char *inp_line, int height, int row_number,
                     const unsigned char block,
                     const unsigned char empty_block) {
    FillWithEmptyBlockChar(inp_line, height, row_number, empty_block);
    FillWithBlockChar(inp_line, height, row_number, block);
}

void MirrorLine(unsigned char *line, unsigned char height,
                unsigned char MAX_WIDTH, unsigned char offset,
                unsigned char offset_char) {
    // append with offset_char
    for (unsigned char i = height; i < height + offset; i++) {
        line[i] = offset_char;
    }
    // loop over the line from the end
    for (unsigned int i = MAX_WIDTH - 1; i > height + offset - 1; i--) {
        // if entry maps to an empty char in the beginning of the
        // string, replace it with a null terminator
        // otherwise, copy it.
        line[i] = (line[(MAX_WIDTH - 1) - i] == ' ')
                      ? '\0'
                      : line[(MAX_WIDTH - 1) - i];
    }
}

void DrawMirroredTriangle(const unsigned char block,
                          const unsigned char empty_block,
                          unsigned char height) {
    for (int row = 0; row < height; row++) {
        unsigned char line[MAX_CHARS * 3] = {0};
        unsigned char width = GetWidth(height, 2, true);
        GetDrawableLine(line, height, row, block, empty_block);
        MirrorLine(line, height, width, 2, empty_block);
        printf("%s", line);
        printf("\n");
    }
}

void DrawTriangle(const unsigned char block, const unsigned char empty_block,
                  unsigned char height) {
    for (int row = 0; row < height; row++) {
        unsigned char line[MAX_CHARS] = {0};
        GetDrawableLine(line, height, row, block, empty_block);
        printf("%s", line);
        printf("\n");
    }
}
