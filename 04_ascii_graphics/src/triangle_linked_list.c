#include "ascii_graphics/triangle_linked_list.h"

Node_t *InitList(void) {
    Node_t *head = (Node_t *)malloc(sizeof(Node_t));
    head->next = NULL;
    head->prev = NULL;
    return head;
}

int DeleteList(Node_t *head) {
    Node_t *cur = head;
    Node_t *next;
    while (cur != NULL) {
        next = cur->next;
        free(cur);
        cur = next;
    }
    return 0;
}

void PushFront(unsigned char value, Node_t **HEAD) {
    Node_t *new_head = (Node_t *)malloc(sizeof(Node_t));
    new_head->character = value;
    (*HEAD)->prev = new_head;
    new_head->next = *HEAD;
    new_head->prev = NULL;
    *HEAD = new_head;
}

unsigned char DeleteByValue(unsigned char value, Node_t *HEAD) {
    Node_t *cur = HEAD;
    Node_t *next;
    unsigned char found = 0;

    while (cur != NULL) {
        next = cur->next;
        if (cur->character == value) {
            found = cur->character;
            free(cur);
        }
        cur = next;
    }
    return found;
}

void PrintList(Node_t *HEAD) {
    Node_t *cur = HEAD;
    while (cur->next != NULL) {
        printf("%c", cur->character);
        cur = cur->next;
    }
    printf("%c\n", cur->character);
}

void SetNodeCharacter(size_t nodeIndex, unsigned char value, Node_t *HEAD) {
    Node_t *cur = HEAD;
    for (volatile size_t i = 0; i < nodeIndex; i++) {
        cur = cur->next;
    }
    cur->character = value;
}

int DrawTriangleLinkedList(unsigned char block, unsigned char empty_block,
                           size_t height) {
    Node_t *list = InitList();
    list->character = block;
    for (size_t i = 0; i < height - 1; i++) {
        PushFront(empty_block, &list);
    }

    printf("Height: %zu\n", height);
    for (size_t row = height; row > 0; --row) {
        SetNodeCharacter(row - 1, block, list);
        PrintList(list);
    }

    if (DeleteList(list) != 0) {
        puts("Failed to delete list.");
    }
    return 0;
}
