/**
 * @file tests.c
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief Tests for ASCII Graphics.
 * @version 0.1
 * @date 2023-02-10
 *
 * @copyright Copyright (c) Mina Ghobrial 2023 under MIT LICENSE
 *
 */
#include "ascii_graphics/mainlib.h"
#include <check.h>

#define BLOCK '#'
#define EMPTY_BLOCK ' '

START_TEST(lines_7_row_0) {
    // number of rows and columns is changed manually with every test
    int num = 7; // NOLINT
    int row = 0; // NOLINT
    unsigned char line[MAX_CHARS] = {0};
    GetDrawableLine(line, num, row, BLOCK, EMPTY_BLOCK);

    ck_assert_int_eq(32, (int)line[0]);
    ck_assert_int_eq(32, (int)line[1]);
    ck_assert_int_eq(32, (int)line[2]);
    ck_assert_int_eq(32, (int)line[3]);
    ck_assert_int_eq(32, (int)line[4]);
    ck_assert_int_eq(32, (int)line[5]);

    ck_assert_int_eq(35, (int)line[6]);
}
END_TEST

START_TEST(lines_8_row_6) {
    // number of rows and columns is changed manually with every test
    int num = 8; // NOLINT
    int row = 6; // NOLINT
    unsigned char line[MAX_CHARS] = {0};
    GetDrawableLine(line, num, row, BLOCK, EMPTY_BLOCK);

    ck_assert_int_eq(32, (int)line[0]);

    ck_assert_int_eq(35, (int)line[1]);
    ck_assert_int_eq(35, (int)line[2]);
    ck_assert_int_eq(35, (int)line[3]);
    ck_assert_int_eq(35, (int)line[4]);
    ck_assert_int_eq(35, (int)line[5]);
    ck_assert_int_eq(35, (int)line[6]);
    ck_assert_int_eq(35, (int)line[7]);
}
END_TEST

static Suite *AsciiGraphics04Suite(void) {
    Suite *suite = NULL;
    TCase *core = NULL;

    suite = suite_create("04_ascii_graphics");

    core = tcase_create("Core");
    tcase_add_test(core, lines_7_row_0);
    tcase_add_test(core, lines_8_row_6);

    suite_add_tcase(suite, core);
    return (suite);
}

int main(void) {
    int failed = 0;
    Suite *suite = NULL;
    SRunner *runner = NULL;

    suite = AsciiGraphics04Suite();
    runner = srunner_create(suite);

    srunner_run_all(runner, CK_NORMAL);
    failed = srunner_ntests_failed(runner);
    srunner_free(runner);

    return (failed == 0 ? EXIT_SUCCESS : EXIT_FAILURE);
}
