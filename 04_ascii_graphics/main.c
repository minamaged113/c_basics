/**
 * @file main.c
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief Draw a half triangle of hashes `#`. The number of rows is given by the
 *  user in the beginning of the run. The maximum number of rows come from the
 *  variable `MAX_CHARS`.
 * @version 0.1
 * @date 2022-08-27
 *
 * @copyright Copyright (c) Mina Ghobrial 2022
 *
 */

#include "ascii_graphics/mainlib.h"
#include "ascii_graphics/triangle_linked_list.h"

/**
 * @brief main Mario program for both less and more comfortable
 *
 * @return int
 */
int main(void) {
    // number of rows & columns
    unsigned char height = (unsigned char)GetUserInput();

    DrawTriangle('#', '-', height);
    DrawMirroredTriangle('#', '-', height);
    DrawTriangleLinkedList('#', '-', height);
    return 0;
}
