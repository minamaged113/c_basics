/**
 * @file main.c
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief Simple substitution cipher example.
 * @version 0.1
 * @date 2022-12-28
 *
 * @copyright Copyright (c) Mina Ghobrial 2022 under MIT LICENSE
 *
 */
#include "substitution/mainlib.h"

int main(int argc, char const *argv[]) {
    char *pc_input_string = NULL;
    char *pc_key = GetKey(argv[1]);
    char *pc_capital_key = Capitalize(pc_key);
    char *pc_capital_input = NULL;

    PrintArgs(argc, argv);

    if (RepeatedLetters(pc_capital_key)) {
        return EXIT_FAILURE;
    }

    if (!ValidKey(argv[1])) {
        return EXIT_FAILURE;
    }

    pc_input_string = GetTextWithMsg("plaintext:\t");
    pc_capital_input = Capitalize(pc_input_string);
    printf("ciphertext:\t%s\n", Encrypt(pc_capital_input, argv[1]));

    free(pc_capital_input);
    free(pc_capital_key);
    free(pc_key);
    free(pc_input_string);
    return EXIT_SUCCESS;
}
