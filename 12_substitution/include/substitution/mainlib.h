/**
 * @file mainlib.h
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief Function declarations for substitution program.
 * @version 0.1
 * @date 2023-02-02
 *
 * @copyright Copyright (c) Mina Ghobrial 2023 under MIT LICENSE
 *
 */
#if !defined(C_BASICS_SUBSTITUTION_MAINLIB_H)
#define C_BASICS_SUBSTITUTION_MAINLIB_H
#include "utils/utils.h"
#include <bsd/string.h> // to use strlcpy
#include <ctype.h>      // to use toupper
#include <stdbool.h>    // to use bools
#include <stdio.h>
#include <stdlib.h> // to use getline

#define MAX_KEY_LENGTH 26
#define GET_TEXT() GetTextWithMsg("")

extern const char kAlphabet[MAX_KEY_LENGTH];

char *GetTextWithMsg(const char *msg);

long GetLetterIndex(char letter);

char *Encrypt(char *inp, const char *c_key);

char *GetKey(const char *commandline);

char *Capitalize(char *c_key);

bool RepeatedLetters(const char *c_key);

bool ValidKey(const char *encryptionKey);

#endif // C_BASICS_SUBSTITUTION_MAINLIB_H
