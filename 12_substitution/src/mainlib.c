/**
 * @file mainlib.c
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief Function definitions for substitution program.
 * @version 0.1
 * @date 2023-02-02
 *
 * @copyright Copyright (c) Mina Ghobrial 2023 under MIT LICENSE
 *
 */
#include "substitution/mainlib.h"

const char kAlphabet[26] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
                            'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
                            'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};

char *GetTextWithMsg(const char *msg) {
    // to be able to return a pointer, the value has to be allocated on the
    // Heap memory. This allows it to be transfareable from one function call
    // to the other.
    // Otherwise, the memory allocated for a function variable will be freed
    // when the execution goes out of the function scope
    size_t n_bufsize = BUFFER_SIZE;
    char *pc_buffer = (char *)calloc(n_bufsize, sizeof(char));
    printf("%s", msg);
    getline(&pc_buffer, &n_bufsize, stdin);

    return pc_buffer;
}

long GetLetterIndex(char letter) {
    for (long i = 0; i < MAX_KEY_LENGTH; i++) {
        if (kAlphabet[i] == letter) {
            return i;
        }
    }
    return -1;
}

char *Encrypt(char *inp, const char *c_key) {
    size_t input_length = strlen(inp);
    char *c_encrypted = (char *)calloc(input_length, sizeof(char));
    char current_letter = '0';
    long letter_index = -1;
    for (size_t i = 0; i < input_length; i++) {
        current_letter = inp[i];
        letter_index = GetLetterIndex(current_letter);
        c_encrypted[i] = c_key[letter_index];
    }
    // c_encrypted = inp;
    return c_encrypted;
}

char *GetKey(const char *commandline) {
    size_t key_length = strlen(commandline);
    char *c_key = (char *)malloc(key_length * sizeof(char));
    strlcpy(c_key, commandline, key_length);
    return c_key;
}

char *Capitalize(char *c_key) {
    char *c_capitals = (char *)malloc(MAX_KEY_LENGTH * sizeof(char));
    for (size_t i = 0; i < strlen(c_key); i++) {
        c_capitals[i] = (char)toupper(c_key[i]);
    }
    return c_capitals;
}

bool RepeatedLetters(const char *c_key) {
    char alphabet[MAX_KEY_LENGTH] = {0};
    strlcpy(alphabet, kAlphabet, MAX_KEY_LENGTH);
    for (size_t letter = 0; letter < MAX_KEY_LENGTH; letter++) {
        for (size_t key_letter = 0; key_letter < MAX_KEY_LENGTH; key_letter++) {
            if (kAlphabet[letter] == c_key[key_letter]) {
                alphabet[letter] = '#';
            }
        }
    }

    for (size_t letter = 0; letter < MAX_KEY_LENGTH; letter++) {
        if (alphabet[letter] != '#') {
            return true;
        }
    }
    return false;
}

bool ValidKey(const char *encryptionKey) {
    bool valid = true;

    // check key length is 26 letters
    if (strlen(encryptionKey) != MAX_KEY_LENGTH) {
        valid = false;
    }

    return valid;
}
