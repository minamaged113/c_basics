# The adventure of the dancing men

One of Sherlock Holmes adventures, where he deciphered drawings of some stick
figures written on a piece of paper. The mysterious sequence of stick men is a
simple substitution cipher; that is, each stick man represents a given letter of
the alphabet.

This program will imitate the same problem but with rearranged alphabet as key.

With a given key of rearranged alphabets, rearrange the letters in a given word
to match the new order.

## References

- https://en.wikipedia.org/wiki/The_Adventure_of_the_Dancing_Men
- https://en.wikipedia.org/wiki/Substitution_cipher