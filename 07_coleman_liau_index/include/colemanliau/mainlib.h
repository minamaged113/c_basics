#if !defined(COLEMANLIAU_H)
#define COLEMANLIAU_H

#include "utils/utils.h"

#include <ctype.h>  // for isalpha
#include <math.h>   // for round
#include <string.h> // for strlen

#define PER100 100
#define LETTERS_COEFFICIENT 0.0588
#define SENTENCES_COEFFICIENT 0.296
#define CONSTANT_COEFFICIENT 15.8

/**
 * @brief Get the Coleman Liau Index Readability Grade of a piece of text.
 * 
 * @param paragraph a piece of input text.
 * @return char The Coleman Liau Index. The range of values if positive
 *  integers from 0.
 */
unsigned char GetColemanLiauIndex(const char paragraph[]);

/**
 * @brief Get the Letters Count in a given text.
 * 
 * @param paragraph a piece of input text.
 * @return unsigned int The number of letters in a given piece of text.
 */
unsigned int GetLettersCount(const char paragraph[]);

/**
 * @brief Get the Words Count.
 * 
 * @param paragraph a piece of input text.
 * @return unsigned int The number of word in given text.
 */
unsigned int GetWordsCount(const char paragraph[]);

/**
 * @brief Get the Sentences Count.
 * 
 * @param paragraph a piece of input text.
 * @return unsigned int The number of scentences in a given piece of text.
 */
unsigned int GetSentencesCount(const char paragraph[]);

/**
 * @brief Get the Coleman Liau Index
 * 
 * @param letters_avg The average number of letters in 100 words.
 * @param sentences_avg The average number of sentence formed by 100 words.
 * @return unsigned char Coleman Liau Index.
 */
unsigned char CalculateColemanLiauIndex(float letters_avg, float sentences_avg);

#endif // COLEMANLIAU_H
