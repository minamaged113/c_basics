#include "colemanliau/mainlib.h"
#include <check.h>

START_TEST(test_01_grade_3) {
    const char paragraph[] = "Congratulations! Today is your day. You're off "
                             "to Great Places! You're off and away!";
    ck_assert_int_eq(65, GetLettersCount(paragraph));
    ck_assert_int_eq(4, GetSentencesCount(paragraph));
    ck_assert_int_eq(14, GetWordsCount(paragraph));
    ck_assert_int_eq(3, GetColemanLiauIndex(paragraph)); // NOLINT
}
END_TEST

START_TEST(test_02_grade_5) {
    const char paragraph[] =
        "Harry Potter was a highly unusual boy in many ways. For one thing, he "
        "hated the summer holidays more than any other time of year. For "
        "another, he really wanted to do his homework, but was forced to do it "
        "in secret, in the dead of the night. And he also happened to be a "
        "wizard.";
    ck_assert_int_eq(214, GetLettersCount(paragraph));
    ck_assert_int_eq(4, GetSentencesCount(paragraph));
    ck_assert_int_eq(56, GetWordsCount(paragraph));
    ck_assert_int_eq(5, GetColemanLiauIndex(paragraph)); // NOLINT
}
END_TEST

START_TEST(test_03_grade_11) {
    const char paragraph[] =
        "As the average number of letters and words per sentence increases, "
        "the Coleman-Liau index gives the text a higher reading level. If you "
        "were to take this paragraph, for instance, which has longer words and "
        "sentences than either of the prior two examples, the formula would "
        "give the text an eleventh grade reading level.";
    ck_assert_int_eq(11, GetColemanLiauIndex(paragraph)); // NOLINT
}
END_TEST

START_TEST(test_04_grade_7) {
    const char paragraph[] =
        "In my younger and more vulnerable years my father gave me some advice "
        "that I've been turning over in my mind ever since";
    ck_assert_int_eq(96, GetLettersCount(paragraph));
    ck_assert_int_eq(23, GetWordsCount(paragraph));
    ck_assert_int_eq(1, GetSentencesCount(paragraph));
    ck_assert_int_eq(7, GetColemanLiauIndex(paragraph)); // NOLINT
}
END_TEST

START_TEST(test_05_grade_1) {
    const char paragraph[] = "One fish. Two fish. Red fish. Blue fish.";
    ck_assert_int_eq(8, GetWordsCount(paragraph));
    ck_assert_int_eq(4, GetSentencesCount(paragraph));
    ck_assert_int_eq(29, GetLettersCount(paragraph));
    ck_assert_int_lt(1, GetColemanLiauIndex(paragraph)); // NOLINT
}
END_TEST

static Suite *ColemanliauTestSuite(void) {
    Suite *suite = NULL;
    TCase *core = NULL;

    suite = suite_create("07_coleman_liau_index");

    core = tcase_create("Core");
    tcase_add_test(core, test_01_grade_3);
    tcase_add_test(core, test_02_grade_5);
    tcase_add_test(core, test_03_grade_11);
    tcase_add_test(core, test_04_grade_7);
    tcase_add_test(core, test_05_grade_1);

    suite_add_tcase(suite, core);
    return (suite);
}

int main(void) {
    int failed = 0;
    Suite *suite = NULL;
    SRunner *runner = NULL;

    suite = ColemanliauTestSuite();
    runner = srunner_create(suite);

    srunner_run_all(runner, CK_NORMAL);
    failed = srunner_ntests_failed(runner);
    srunner_free(runner);

    return (failed == 0 ? EXIT_SUCCESS : EXIT_FAILURE);
}
