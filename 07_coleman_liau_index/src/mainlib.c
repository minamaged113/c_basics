#include "colemanliau/mainlib.h"

unsigned char GetColemanLiauIndex(const char paragraph[]) {
    // analyze piece of text
    size_t letters_in_paragraph = GetLettersCount(paragraph);
    size_t scentences_in_paragraph = GetSentencesCount(paragraph);
    size_t words_in_paragraph = GetWordsCount(paragraph);

    // calculate letters average in 100 words
    float ltrs_avg =
        ((float)letters_in_paragraph / (float)words_in_paragraph) * PER100;
    // calculate scentences average in 100 words
    float sntncs_avg =
        ((float)scentences_in_paragraph / (float)words_in_paragraph) * PER100;

    // calculate Coleman Liau Index
    unsigned char uc_coleman_liau_index =
        CalculateColemanLiauIndex(ltrs_avg, sntncs_avg);

    return uc_coleman_liau_index;
}

unsigned int GetLettersCount(const char paragraph[]) {
    // calculate length of text
    size_t n_length = strlen(paragraph);
    unsigned int count = 0;

    for (size_t i = 0; i < n_length; i++) {
        if (isalpha(paragraph[i])) {
            count++;
        }
    }

    return count;
}

unsigned int GetWordsCount(const char paragraph[]) {
    // calculate length of text
    size_t n_length = strlen(paragraph);
    unsigned int un_words = 1;
    for (size_t i = 0; i < n_length; i++) {
        if (paragraph[i] == ' ') {
            un_words++;
        }
    }

    return un_words;
}

unsigned int GetSentencesCount(const char paragraph[]) {
    // calculate length of text
    size_t n_length = strlen(paragraph);
    unsigned int n_sentences = 0;
    for (size_t i = 0; i < n_length; i++) {
        if (ispunct(paragraph[i])) {
            if (paragraph[i] == '.' || paragraph[i] == '!' ||
                paragraph[i] == '?') {
                n_sentences++;
            }
        }
    }
    if (n_length > 1 && n_sentences == 0) {
        n_sentences++;
    }
    return n_sentences;
}

unsigned char CalculateColemanLiauIndex(float letters_avg,
                                        float sentences_avg) {
    double ltrs_part = LETTERS_COEFFICIENT * (double)letters_avg;
    double sntncs_part = SENTENCES_COEFFICIENT * (double)sentences_avg;
    double index = round(ltrs_part - sntncs_part - CONSTANT_COEFFICIENT);
    return (index < 1) ? 0 : (unsigned char)index;
}
