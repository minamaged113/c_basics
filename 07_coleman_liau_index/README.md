# Coleman–Liau Index

The Coleman–Liau index is a readability test designed by Meri Coleman and T. L.
Liau to gauge the understandability of a text.

## The Formula

The Coleman–Liau index is calculated with the following formula:

```
CLI = 0.0588L − 0.296S − 15.8
```

Where L is the average number of letters per 100 words and S is the average
number of sentences per 100 words.

## References

- https://en.wikipedia.org/wiki/Coleman%E2%80%93Liau_index
