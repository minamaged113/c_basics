/**
 * @file main.c
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief This program determines comprehensibility level of a piece of text
 * @version 0.1
 * @date 2021-10-24
 *
 * @details This program should take a text and determines its comprehensibility
 * level. For example, if user types in a line from Dr. Seuss:
 * $ ./colemanliau
 * Text: Congratulations! Today is your day. You're off to Great Places! You're
 *  off and away!
 * Grade 3
 *
 */

#include "colemanliau/mainlib.h"

int main(void) {
    GetColemanLiauIndex(
        "In my younger and more vulnerable years my father gave me some advice "
        "that I've been turning over in my mind ever since");
    return EXIT_SUCCESS;
}
