# Cashier

This application uses an input amount of change and determines the least amount
of coins needed to give back.

## Example

If a customer is paying for a product with cash, and the amount of cash given
exceeds the price. The difference in price between the item's price and the paid
amount is input to this program and it decides the lowest number of coins to
return to the paying customer.

For instance, if the change owed after payment is 41 cents, the number of coins
returned to the paying customer is 4, 1 quarter (25 cents), 1 dime (10 cents),
1 nickle (5 cents), and 1 penny (1 cent). The resulting total is then 41

## More information

1. Greedy algorithms