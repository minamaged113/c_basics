/**
 * @file main.c
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief A greedy virtual cashier.
 * @version 0.1
 * @date 2022-12-27
 *
 * @copyright Copyright (c) Mina Ghobrial 2022 under MIT LICENSE
 *
 */

#include "cashier/mainlib.h"

/**
 * @brief Main cash program functionality
 *
 * @return int Returns 0 in case of successful execution only.
 */
int main(void) { return Cash(); }
