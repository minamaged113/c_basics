#if !defined(CASHIER_MAINLIB_H)
#define CASHIER_MAINLIB_H

#define QUARTERS_VALUE 25
#define DIMES_VALUE 10
#define NICKLES_VALUE 5
#define PENNIES_VALUE 1

#include "utils/utils.h"

/**
 * @brief Calculate the amount of quarters that can be subtracted from
 *  a given cash amount
 *
 * @param payment Cash payment received
 * @return int Number of quarters that can be subtracted from the payment
 */
unsigned int CalculateQuarters(unsigned int payment);

/**
 * @brief Calculate the amount of dimes that can be subtracted from
 *  a given cash amount
 *
 * @param payment Cash payment received
 * @return unsigned int Number of dimes that can be subtracted from the payment
 */
unsigned int CalculateDimes(unsigned int payment);

/**
 * @brief Calculate the amount of nickles that can be subtracted from
 *  a given cash amount
 *
 * @param payment Cash payment received
 * @return unsigned int Number of nickles that can be subtracted from the
 * payment
 */
unsigned int CalculateNickles(unsigned int payment);

/**
 * @brief Calculate the amount of pennies that can be subtracted from
 *  a given cash amount
 *
 * @param payment Cash payment received
 * @return unsigned int Number of pennies that can be subtracted from the
 * payment
 */
unsigned int CalculatePennies(unsigned int payment);

/**
 * @brief Get the greedy lowest number of coins that can make a given
 *  amount of payment
 *
 * @param payment A given number that represents a cash payment
 * @return int The least number of coins that can make up the given cash
 *  amount
 */
unsigned int GetCoins(unsigned int payment);

/**
 * @brief Run program main function
 *
 * @return int return 0 if success
 */
int Cash(void);

#endif // CASHIER_MAINLIB_H
