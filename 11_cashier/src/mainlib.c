#include "cashier/mainlib.h"

unsigned int CalculateQuarters(unsigned int payment) {
    return payment / QUARTERS_VALUE;
}

unsigned int CalculateDimes(unsigned int payment) {
    return payment / DIMES_VALUE;
}

unsigned int CalculateNickles(unsigned int payment) {
    return payment / NICKLES_VALUE;
}

unsigned int CalculatePennies(unsigned int payment) {
    return payment / PENNIES_VALUE;
}

unsigned int GetCoins(unsigned int payment) {
    unsigned int coins = 0; // current number of coins
    unsigned int dimes = 0; // current number of dimes
    unsigned int nickles = 0; // current number of nickles
    unsigned int pennies = 0; // current number of pennies

    // Calculate number of quarters
    unsigned int quarters = CalculateQuarters(payment);
    coins += quarters;
    payment = (payment - (quarters * QUARTERS_VALUE));

    // Calculate number of dimes
    dimes = CalculateDimes(payment);
    coins += dimes;
    payment = (payment - (dimes * DIMES_VALUE));

    // Calculate number of nickles
    nickles = CalculateNickles(payment);
    coins += nickles;
    payment = (payment - (nickles * NICKLES_VALUE));

    // Calculate number of pennies
    pennies = CalculatePennies(payment);
    coins += pennies;

    return coins; // return accumulated least number of coins
}

int Cash(void) {
    unsigned int n_change_owed = GetInt();
    printf("Number of coins is:  %d\n", GetCoins(n_change_owed));
    return EXIT_SUCCESS;
}
