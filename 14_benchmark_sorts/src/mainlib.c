/**
 * @file mainlib.c
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief Sorting benchmarks function definitions.
 * @version 0.1
 * @date 2023-03-16
 *
 * @copyright Copyright (c) Mina Ghobrial 2023 under MIT LICENSE
 *
 */

#include "benchmark_sorts/mainlib.h"

void FillArrayAscendingly(int32_t *arr, int32_t size) {
    for (int32_t i = 1; i <= size; i++) {
        arr[i - 1] = i;
    }
    return;
}

void FillArrayDescendingly(int32_t *arr, int32_t size) {
    for (int32_t i = size; i > 0; i--) {
        arr[size - i] = i;
    }
    return;
}

double RunTimeAnalysisOn(void (*f)(int32_t *, uint64_t), int32_t *arr,
                         uint64_t size) {
    int64_t click = clock();
    (*f)(arr, size);
    click = clock() - click;

    return (double)click / CLOCKS_PER_SEC;
}
