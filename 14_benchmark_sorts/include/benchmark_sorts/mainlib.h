/**
 * @file mainlib.h
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief Sorting benchmarks functions declarations
 * @version 0.1
 * @date 2023-03-16
 *
 * @copyright Copyright (c) Mina Ghobrial 2023 under MIT LICENSE
 *
 */

#if !defined(BENCHMARK_SORTS_MAINLIB_H)
#define BENCHMARK_SORTS_MAINLIB_H

#include "utils/logging.h"
#include "utils/sort.h"

#include <time.h>

#define ARRAY_SIZE 50000UL

void FillArrayAscendingly(int32_t *arr, int32_t size);
void FillArrayDescendingly(int32_t *arr, int32_t size);
double RunTimeAnalysisOn(void (*f)(int32_t *, uint64_t), int32_t *arr,
                         uint64_t size);

#endif // BENCHMARK_SORTS_MAINLIB_H
