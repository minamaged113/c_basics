# Benchmark Sorts

Benchmark sorting algorithms to show the timing complexity of each. Benchmarks
are computed on basis of:

- Best case scenario --> array is already sorted.
- Worst case scenario --> array is reversed.

## References

- https://en.wikipedia.org/wiki/Time_complexity
- https://www.freecodecamp.org/news/big-o-cheat-sheet-time-complexity-chart/
