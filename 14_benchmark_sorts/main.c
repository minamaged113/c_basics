/**
 * @file main.c
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief Run the sort benchmarks.
 * @version 0.1
 * @date 2023-03-16
 *
 * @copyright Copyright (c) Mina Ghobrial 2023 under MIT LICENSE
 *
 */

#include "benchmark_sorts/mainlib.h"

int main(void) {
    double bcs_merge_sort_time = 0;
    double bcs_bubble_sort_time = 0;
    double bcs_selection_sort_time = 0;
    double wcs_merge_sort_time = 0;
    double wcs_bubble_sort_time = 0;
    double wcs_selection_sort_time = 0;

    int32_t sorted_array[ARRAY_SIZE];
    int32_t reversed_array[ARRAY_SIZE];

    FillArrayAscendingly(sorted_array, ARRAY_SIZE);
    FillArrayDescendingly(reversed_array, ARRAY_SIZE);

    //**************************************************************************
    //******************************************************* Best case scenario
    // Merge Sort
    bcs_merge_sort_time =
        RunTimeAnalysisOn(MergeSort, sorted_array, ARRAY_SIZE);
    LogInfo("BCS | MS |\t%.4f", bcs_merge_sort_time);

    // Bubble Sort
    bcs_bubble_sort_time =
        RunTimeAnalysisOn(BubbleSort, sorted_array, ARRAY_SIZE);
    LogInfo("BCS | BS |\t%.4f", bcs_bubble_sort_time);

    // Selection Sort
    bcs_selection_sort_time =
        RunTimeAnalysisOn(SelectionSort, sorted_array, ARRAY_SIZE);
    LogInfo("BCS | SS |\t%.4f", bcs_selection_sort_time);

    //**************************************************************************
    //***************************************************** Worst case scenarios
    // Merge Sort
    wcs_merge_sort_time =
        RunTimeAnalysisOn(MergeSort, reversed_array, ARRAY_SIZE);
    LogInfo("WCS | MS |\t%.4f", wcs_merge_sort_time);

    // Bubble Sort
    FillArrayDescendingly(reversed_array, ARRAY_SIZE);
    wcs_bubble_sort_time =
        RunTimeAnalysisOn(BubbleSort, reversed_array, ARRAY_SIZE);
    LogInfo("WCS | BS |\t%.4f", wcs_bubble_sort_time);

    FillArrayDescendingly(reversed_array, ARRAY_SIZE);
    wcs_selection_sort_time =
        RunTimeAnalysisOn(SelectionSort, reversed_array, ARRAY_SIZE);
    LogInfo("WCS | SS |\t%.4f", wcs_selection_sort_time);

    return EXIT_SUCCESS;
}
