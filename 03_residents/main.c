/**
 * @file main.c
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief Determine how long it takes for a city's population to reach a given
 *  size.
 * @details Calculate the number of residents in a given city according to a
 * predefined equation. A predefined equation determines the passing-away rate
 * and birth-rate of residents.
 *
 * @copyright Copyright (c) Mina Ghobrial 2021 under MIT LICENSE
 *
 */
#include "residents/mainlib.h"

int main(void) {
    long start = 0;
    long end = 0;
    char c_input[BUFFER_SIZE] = {0};
    char *ptr = NULL;
    // Prompt for start size
    do {
        printf("Start size: ");
        if (fgets(c_input, BUFFER_SIZE, stdin) != NULL) {
            start = strtol(c_input, &ptr, kBase10);
        }
    } while (start < kSmallestStartSize);

    // Prompt for end size
    do {
        printf("End size: ");
        if (fgets(c_input, BUFFER_SIZE, stdin) != NULL) {
            end = strtol(c_input, &ptr, kBase10);
        }
    } while (end < start);
    // Print number of years
    printf("Years: %ld\n", GetDuration(start, end));
}
