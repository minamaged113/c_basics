# Residents

Calculate the number of residents in a given city according to a predefined
equation. A predefined equation determines the passing-away rate and birth-rate
of residents.

Given the starting number of current residents of a city, then the target
number of residents, the program is able to calculate the number of years that 
it would take for the number of residents to cross the target number.

## Sample run

```bash
./program
Start size: 5000    # input
End size: 12000     # input
Years: 11           # output
```