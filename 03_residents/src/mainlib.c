/**
 * @file mainlib.c
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief Function definitions related to residents status.
 * @version 0.1
 * @date 2023-02-10
 *
 * @copyright Copyright (c) Mina Ghobrial 2023 under MIT LICENSE
 *
 */
#include "residents/mainlib.h"

void ResidentsUpdate(long *pn_current_residents) {
    *pn_current_residents = *pn_current_residents +
                            (*pn_current_residents / 3) -
                            (*pn_current_residents / 4);
}

long GetDuration(long start, long end) {
    long years = 0;

    if ((start < kSmallestStartSize) | (start > end)) {
        return EXIT_FAILURE;
    }
    // Calculate number of years until we reach threshold
    while (start < end) {
        ResidentsUpdate(&start);
        years++;
    }
    return years;
}
