/**
 * @file tests.c
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief Testing the program functions.
 * @version 0.1
 * @date 2023-02-10
 *
 * @copyright Copyright (c) Mina Ghobrial 2023 under MIT LICENSE
 *
 */
#include "residents/mainlib.h"
#include <check.h>

START_TEST(values_less_than_9) {
    ck_assert_int_eq(EXIT_FAILURE, GetDuration(1, 10));
    ck_assert_int_eq(EXIT_FAILURE, GetDuration(3, 10));
    ck_assert_int_eq(EXIT_FAILURE, GetDuration(5, 10));
    ck_assert_int_eq(EXIT_FAILURE, GetDuration(7, 10));
}
END_TEST

START_TEST(values_less_than_starting_values) {
    ck_assert_int_eq(EXIT_FAILURE, GetDuration(10, 9));
    ck_assert_int_eq(EXIT_FAILURE, GetDuration(13, 10));
    ck_assert_int_eq(EXIT_FAILURE, GetDuration(17, 11));
    ck_assert_int_eq(EXIT_FAILURE, GetDuration(11, 7));
}
END_TEST

START_TEST(decimal_number) { ck_assert_int_eq(2, GetDuration(1100, 1192)); }
END_TEST

START_TEST(same_starting_and_ending_sizes) {
    ck_assert_int_eq(0, GetDuration(11, 11));
}
END_TEST

START_TEST(handles_negative_residents) {
    ck_assert_int_eq(EXIT_FAILURE, GetDuration(-4, -50));
}
END_TEST

START_TEST(handles_starting_100) {
    ck_assert_int_eq(115, GetDuration(100, 1000000));
}
END_TEST

static Suite *Residents03Suite(void) {
    Suite *suite = NULL;
    TCase *core = NULL;

    suite = suite_create("03_residents");

    core = tcase_create("Core");
    tcase_add_test(core, values_less_than_9);
    tcase_add_test(core, values_less_than_starting_values);
    tcase_add_test(core, decimal_number);
    tcase_add_test(core, same_starting_and_ending_sizes);
    tcase_add_test(core, handles_negative_residents);
    tcase_add_test(core, handles_starting_100);

    suite_add_tcase(suite, core);
    return (suite);
}

int main(void) {
    int failed = 0;
    Suite *suite = NULL;
    SRunner *runner = NULL;

    suite = Residents03Suite();
    runner = srunner_create(suite);

    srunner_run_all(runner, CK_NORMAL);
    failed = srunner_ntests_failed(runner);
    srunner_free(runner);

    return (failed == 0 ? EXIT_SUCCESS : EXIT_FAILURE);
}
