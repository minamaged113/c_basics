/**
 * @file mainlib.h
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief Functions related to residents status.
 * @version 0.1
 * @date 2023-02-10
 * 
 * @copyright Copyright (c) Mina Ghobrial 2023 under MIT LICENSE
 * 
 */
#ifndef RESIDENTS_MAINLIB_H
#define RESIDENTS_MAINLIB_H

#include "utils/utils.h"

typedef enum {
    kSmallestStartSize = 9
} Residents;

/**
 * @brief Update a given number of residents with n = n + n/3 - n/4.
 * 
 * @param n_current_residents Reference to the current number of residents. This
 *      variable is changed as a side effect.
 */
void ResidentsUpdate(long *n_current_residents);

/**
 * @brief Get the number of years it takes to reach a target number of 
 *      residents.
 * 
 * @param start starting number or residents.
 * @param end ending number of residents.
 * @return long Number of years needed to reach end number of residents.
 * returns EXIT_FAILURE if the number of starting residents is less than 9 or 
 * the start entry is less than the end entry.
 */
long GetDuration(long start, long end);
#endif // RESIDENTS_MAINLIB_H
