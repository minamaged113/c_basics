#include "luhn/mainlib.h"

void SplitNumberIntoDigits(unsigned char *digits, int *counter, long num) {
    // digits : empty array of ints that will hold the digits
    // counter : pointer to an int that will hold the number of digits
    int count = 0;
    // split the number into its digits
    // digits are stored reversed into the array "digits"
    while (num > 0) // do till num greater than  0
    {
        unsigned char mod =
            (unsigned char)(num % kBase10); // split last digit from number
        // printf("%d\n",mod); //print the digit.
        digits[count] = mod;

        count++; // this keeps track of the number of digits available
        num = num / kBase10; // divide num by 10. num /= 10 also a valid one
    }
    *counter = count;
}

void PrintNumberReversed(char *digits, int count) {
    for (int i = 0; i < count; i++) {
        printf("%i", digits[i]);
    }
    printf("\n");
}

void PrintNumber(char *digits, int count) {
    for (int i = count; i > 0; i--) {
        printf("%i", digits[i - 1]);
    }
    printf("\n");
}

unsigned char SumDigits(unsigned char number_more_than_1_digit) {
    // maximum input to this function should be a 2 digit number
    // max 2*9 = 18
    // min 2*5 = 10
    unsigned char digits[2] = {0};
    int count = 0;

    SplitNumberIntoDigits(digits, &count, number_more_than_1_digit);

    return digits[0] + digits[1];
}

unsigned int MulEveryOtherDigitBy2SumDigits(const unsigned char *digits,
                                            int count) {
    unsigned int sum = 0;
    unsigned char current_digit = 0;
    unsigned char multiplied_by_2 = 0;

    for (int i = 1; i < count; i += 2) {
        current_digit = digits[i];
        multiplied_by_2 = current_digit * 2;

        if (multiplied_by_2 > HIGHEST_DIGIT) {
            sum += (SumDigits(multiplied_by_2));
        } else {
            sum += (multiplied_by_2);
        }
    }

    // printf("sum: %d\n", sum);
    return sum;
}

unsigned char SumOfNotMultipliedBy2(const unsigned char *digits, int count) {
    unsigned char sum = 0;
    unsigned char current_number = 0;
    for (int i = 0; i < count; i += 2) {
        // printf("%d\n", digits[i-1]);
        current_number = digits[i];
        sum += current_number;
    }

    return sum;
}

bool CheckNumberInArrayUsingIndex(const unsigned char *digits,
                                  unsigned char index,
                                  unsigned char reference_value) {
    bool ret = false;
    if (digits[index] == reference_value) {
        ret = true;
    }
    return ret;
}

bool DecideCard(unsigned char *digits, int count) {
    // American Express:
    // 15-digits
    // start with 34 or 37

    // the numbers are stored inversed in the array, so instead
    // of indexing from 0, we index from count-1
    unsigned char first_number = (unsigned char)(count - 1);
    unsigned char second_number = (unsigned char)(count - 2);
    bool found = false;
    if ((count == AMEX_DIGITS) &&
        CheckNumberInArrayUsingIndex(digits, first_number, AMEX_FIRST_DIGIT) &&
        (CheckNumberInArrayUsingIndex(digits, second_number,
                                      AMEX_SECOND_DIGIT_4) ||
         CheckNumberInArrayUsingIndex(digits, second_number,
                                      AMEX_SECOND_DIGIT_7))) {
        puts("AMEX");
        found = true;
    }

    // Visa:
    // 13-digits or 16-digits
    // starts with 4
    else if ((count == VISA_DIGITS_13) &
             CheckNumberInArrayUsingIndex(digits, first_number,
                                          VISA_FIRST_DIGIT)) {
        puts("VISA");
        found = true;
    }
    // Master Cards:
    // 16-digits
    // start with 51, 52, 53, 54, or 55
    else if (count == VISA_DIGITS_16) {
        // this can either be master card or Visa
        if (CheckNumberInArrayUsingIndex(digits, first_number,
                                         VISA_FIRST_DIGIT)) {
            puts("VISA");
            found = true;
        } else if (CheckNumberInArrayUsingIndex(digits, first_number,
                                                MASTERCARD_FIRST_DIGIT) &&
                   (CheckNumberInArrayUsingIndex(digits, second_number,
                                                 MASTERCARD_SECOND_DIGIT_1) ||
                    CheckNumberInArrayUsingIndex(digits, second_number,
                                                 MASTERCARD_SECOND_DIGIT_2) ||
                    CheckNumberInArrayUsingIndex(digits, second_number,
                                                 MASTERCARD_SECOND_DIGIT_3) ||
                    CheckNumberInArrayUsingIndex(digits, second_number,
                                                 MASTERCARD_SECOND_DIGIT_4) ||
                    CheckNumberInArrayUsingIndex(digits, second_number,
                                                 MASTERCARD_SECOND_DIGIT_5))) {
            puts("MASTERCARD");
            found = true;
        }
    }
    return found;
}
