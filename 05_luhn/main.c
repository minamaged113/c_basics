#include "luhn/mainlib.h"

int main(void) {
    long num = 0;               // the credit card number under inspection
    int count = 0;              // this will keep track of the number of digits
    unsigned int mul_2_sum = 0; // holds the value of the sum of all the digits
                                // of numbers multiplied by 2
    unsigned char not_mul_2_sum = 0;
    unsigned int luhn_sum = 0;
    char unsigned digits[MAX] = {
        0}; // this array will hold the digits when separated

    char c_input[BUFFER_SIZE] = {0};
    char *ptr = NULL;

    // get the number from the user
    if (fgets(c_input, BUFFER_SIZE, stdin) != NULL) {
        num = strtol(c_input, &ptr, kBase10);
    }
    // fill the array `digits`
    // with the digits from `num`
    // and count them in `count`
    SplitNumberIntoDigits(digits, &count, num);

    mul_2_sum = MulEveryOtherDigitBy2SumDigits(digits, count);
    not_mul_2_sum = SumOfNotMultipliedBy2(digits, count);
    luhn_sum = mul_2_sum + (unsigned int)not_mul_2_sum;

    if (luhn_sum % kBase10 == 0) {
        DecideCard(digits, count);
        return 0;
    }

    puts("INVALID");
    return 0;
}
