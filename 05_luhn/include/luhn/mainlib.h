#if !defined(LUHN_MAINLIB_H)
#define LUHN_MAINLIB_H

#include "utils/utils.h"

#include <stdbool.h>
#include <stdio.h>

#define MAX 17
#define HIGHEST_DIGIT 9

#define AMEX_DIGITS 15
#define AMEX_FIRST_DIGIT 3
#define AMEX_SECOND_DIGIT_4 4
#define AMEX_SECOND_DIGIT_7 7

#define VISA_DIGITS_13 13
#define VISA_DIGITS_16 16
#define VISA_FIRST_DIGIT 4

#define MASTERCARD_DIGITS 16
#define MASTERCARD_FIRST_DIGIT 5
#define MASTERCARD_SECOND_DIGIT_1 1
#define MASTERCARD_SECOND_DIGIT_2 2
#define MASTERCARD_SECOND_DIGIT_3 3
#define MASTERCARD_SECOND_DIGIT_4 4
#define MASTERCARD_SECOND_DIGIT_5 5

void SplitNumberIntoDigits(unsigned char *digits, int *count, long num);
void PrintNumberReversed(char *digits, int count);
void PrintNumber(char *digits, int count);
unsigned char SumDigits(unsigned char number_more_than_1_digit);
unsigned int MulEveryOtherDigitBy2SumDigits(const unsigned char *digits,
                                            int count);
unsigned char SumOfNotMultipliedBy2(const unsigned char *digits, int count);
bool DecideCard(unsigned char *digits, int count);
bool CheckNumberInArrayUsingIndex(const unsigned char *digits,
                                  unsigned char index,
                                  unsigned char reference_value);

#endif // LUHN_MAINLIB_H
