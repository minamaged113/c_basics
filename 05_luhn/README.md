# Hans Peter Luhn's

Hans Peter Luhn has created an algorithm which is currently used as a simple
checksum to validate many identification numbers.

The programs check a credit card number if valid or not. If the card number is
valid, check what is the card operator (Visa, MasterCard, ...).

The card number is validated with Luhn’s Algorithm.

## References

- https://en.wikipedia.org/wiki/Luhn_algorithm
- https://www.groundlabs.com/blog/anatomy-of-a-credit-card/