#include "shift_cipher/mainlib.h"

int main(int argc, char const *argv[]) {
    char plaintext[MAX_SIZE];
    char ciphertext[MAX_SIZE];
    char *ptr = NULL;
    long cipher_key;
    size_t fgets_strlen;

    if (AnalyzeInput(argc, argv)) {
        return EXIT_FAILURE;
    }

    cipher_key = strtol(argv[1], &ptr, kBase10);

    printf("plaintext: ");
    fgets(plaintext, MAX_SIZE, stdin);

    fgets_strlen = FgetsStrlen(plaintext);

    ShiftCipher(plaintext, ciphertext, fgets_strlen, cipher_key);
    printf("ciphertext: ");
    for (size_t i = 0; i < fgets_strlen; i++) {
        printf("%c", ciphertext[i]);
    }
    printf("\n");
    return EXIT_SUCCESS;
}
