#if !defined(SHIFT_CIPHER_MAINLIB_H)
#define SHIFT_CIPHER_MAINLIB_H

#include "utils/utils.h"

#include <ctype.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_SIZE 256
#define ALPHABET_LENGTH 26
#define CORRECT_ARGC 2
#define ERROR_USAGE "Usage: ./prog key\n"

/**
 * @brief Encrypt a given string using shift cipher.
 *
 * @param source Given string.
 * @param dst Output string.
 * @param str_len Length of given string.
 * @param cipher_key Shift parameter.
 */
void ShiftCipher(const char *source, char *dst, size_t str_len,
                 long cipher_key);

int AnalyzeInput(int argc, const char *argv[]);

size_t FgetsStrlen(const char str[]);

#endif // SHIFT_CIPHER_MAINLIB_H
