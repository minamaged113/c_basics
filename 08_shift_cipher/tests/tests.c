#include "shift_cipher/mainlib.h"
#include <check.h>

START_TEST(encrypts_a_as_b_using_1_as_key) {
    long key = 1; // NOLINT
    const char *src = (const char *)"a";
    const char *expected = (const char *)"b";
    char result[MAX_SIZE];

    ShiftCipher(src, result, strlen(src), key);

    for (size_t i = 0; i < strlen(src); i++) {
        ck_assert_int_eq(expected[i], result[i]); // NOLINT
    }
}
END_TEST
START_TEST(encrypts_barfoo_as_yxocll_using_23_as_key) {
    long key = 23; // NOLINT
    const char *src = "barfoo";
    const char *expected = "yxocll";
    char result[MAX_SIZE];

    ShiftCipher(src, result, strlen(src), key);

    for (size_t i = 0; i < strlen(src); i++) {
        ck_assert_int_eq(expected[i], result[i]); // NOLINT
    }
}
END_TEST
START_TEST(encrypts_BARFOO_as_EDUIRR_using_3_as_key) {
    long key = 3;
    const char *src = "BARFOO";
    const char *expected = "EDUIRR";
    char result[MAX_SIZE];
    ShiftCipher(src, result, strlen(src), key);

    for (size_t i = 0; i < strlen(src); i++) {
        ck_assert_int_eq(expected[i], result[i]); // NOLINT
    }
}
END_TEST
START_TEST(encrypts_BaRFoo_as_FeVJss_using_4_as_key) {
    long key = 4; // NOLINT
    const char *src = "BaRFoo";
    const char *expected = "FeVJss";
    char result[MAX_SIZE];
    ShiftCipher(src, result, strlen(src), key);

    for (size_t i = 0; i < strlen(src); i++) {
        ck_assert_int_eq(expected[i], result[i]); // NOLINT
    }
}
END_TEST
START_TEST(encrypts_barfoo_as_onesbb_using_65_as_key) {
    long key = 65; // NOLINT
    const char *src = "barfoo";
    const char *expected = "onesbb";
    char result[MAX_SIZE];
    ShiftCipher(src, result, strlen(src), key);

    for (size_t i = 0; i < strlen(src); i++) {
        ck_assert_int_eq(expected[i], result[i]); // NOLINT
    }
}
END_TEST

START_TEST(encrypts_world_say_hello_as_iadxp_emk_tqxxa_using_12_as_key) {
    long key = 12; // NOLINT
    const char *src = "world, say hello!";
    const char *expected = "iadxp, emk tqxxa!";
    char result[MAX_SIZE];
    ShiftCipher(src, result, strlen(src), key);

    for (size_t i = 0; i < strlen(src); i++) {
        ck_assert_int_eq(expected[i], result[i]); // NOLINT
    }
}
END_TEST

static Suite *CaesarTestSuite(void) {
    Suite *suite = NULL;
    TCase *core = NULL;

    suite = suite_create("08_caesar");

    core = tcase_create("Core");
    tcase_add_test(core, encrypts_a_as_b_using_1_as_key);
    tcase_add_test(core, encrypts_barfoo_as_yxocll_using_23_as_key);
    tcase_add_test(core, encrypts_BARFOO_as_EDUIRR_using_3_as_key);
    tcase_add_test(core, encrypts_BaRFoo_as_FeVJss_using_4_as_key);
    tcase_add_test(core, encrypts_barfoo_as_onesbb_using_65_as_key);
    tcase_add_test(core,
                   encrypts_world_say_hello_as_iadxp_emk_tqxxa_using_12_as_key);

    suite_add_tcase(suite, core);
    return (suite);
}

int main(void) {
    int failed = 0;
    Suite *suite = NULL;
    SRunner *runner = NULL;

    suite = CaesarTestSuite();
    runner = srunner_create(suite);

    srunner_run_all(runner, CK_NORMAL);
    failed = srunner_ntests_failed(runner);
    srunner_free(runner);

    return (failed == 0 ? EXIT_SUCCESS : EXIT_FAILURE);
}
