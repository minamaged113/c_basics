# Shift Cipher

It is a type of substitution cipher in which each letter in the plaintext is
replaced by a letter some fixed number of positions down the alphabet.

## References

- https://en.wikipedia.org/wiki/Caesar_cipher