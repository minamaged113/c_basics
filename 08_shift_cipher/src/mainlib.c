#include "shift_cipher/mainlib.h"

static bool ContainsAlpha(const char arr[], size_t arr_len) {
    for (size_t i = 0; i < arr_len; i++) {

        if (isalpha(arr[i])) {
            return EXIT_FAILURE;
        }
    }

    return EXIT_SUCCESS;
}

static bool WrongCommandInvoke(const int l_value, const int r_value) {
    return l_value == r_value ? true : false;
}

static bool KeyIsNotNumber(const char *key) {
    bool is_alpha = false;
    if (ContainsAlpha(key, strlen(key))) {
        is_alpha = true;
    }
    return is_alpha;
}

int AnalyzeInput(int argc, const char *argv[]) {

    if (WrongCommandInvoke(argc, CORRECT_ARGC) || KeyIsNotNumber(argv[1])) {
        printf(ERROR_USAGE);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

size_t FgetsStrlen(const char str[]) {
    for (size_t i = 0; i < MAX_SIZE; i++) {
        if (str[i] == '\n') {
            return i;
        }
    }

    return 0;
}

void ShiftCipher(const char *source, char *dst, size_t str_len,
                 long cipher_key) {
    int tmp_encoded_char = '0';
    long alphabet_rounds = 1;
    if (cipher_key > ALPHABET_LENGTH) {
        alphabet_rounds = cipher_key / (long)ALPHABET_LENGTH;
        cipher_key -= ALPHABET_LENGTH * alphabet_rounds;
    }
    for (size_t i = 0; i < str_len; i++) {
        if (isalpha(source[i])) {
            tmp_encoded_char = source[i] + (char)cipher_key;
            if (isupper(source[i])) {
                if (tmp_encoded_char > 'Z') {
                    tmp_encoded_char -= ALPHABET_LENGTH;
                }
            } else {
                if (tmp_encoded_char > 'z') {
                    tmp_encoded_char -= ALPHABET_LENGTH;
                }
            }
            dst[i] = (char)tmp_encoded_char;
        } else {
            dst[i] = source[i];
        }
    }
}
