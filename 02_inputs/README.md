# Handling Inputs

A simple program that handles simple user inputs. It takes 2 space-separated
integers in one line and 2 space-separated floats in another. The program then
outputs in the first line the sum and the difference of the 2 integers
respectively. In the second output line, it prints the sum and the difference of
the 2 given floats respectively.

## Sample run

```bash
./program
1 2         # input
3.4 5.6     # input
3 -1        # output
9.0 -2.2    # output
```
