/**
 * @file main.c
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief A simple program to handle simple user inputs.
 *
 * This program takes 4 numbers, 2 ints in the first line, and 2 floats
 * on the second. It returns the sum and difference between both on 2
 * consecutive lines.
 *
 * @date 2021-09-05
 *
 * @copyright Copyright (c) Mina Ghobrial 2022 under MIT LICENSE
 *
 */

#include "utils/utils.h"

/**
 * @brief The main program function.
 *
 * @return int
 */
int main(void) {
    long n_augend = 0;
    long n_addend = 0;
    float f_augend = 0;
    float f_addend = 0;
    char c_input[BUFFER_SIZE] = {0};
    char *ptr = NULL;

    if (fgets(c_input, BUFFER_SIZE, stdin) != NULL) {
        n_addend = strtol(c_input, &ptr, kBase10);
        n_augend = strtol(c_input, &ptr, kBase10);
    }
    if (fgets(c_input, BUFFER_SIZE, stdin) != NULL) {
        f_addend = strtof(c_input, &ptr);
        f_augend = strtof(c_input, &ptr);
    }

    printf("%ld %ld\n", Add(n_addend, n_augend), Sub(n_addend, n_augend));

    printf("%.1f %.1f\n", AddFloats(f_addend, f_augend),
           SubFloats(f_addend, f_augend));

    return EXIT_SUCCESS;
}
