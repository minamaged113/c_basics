# Projects

[01 - Hello World](./01_hello_world/REAMDE.md)

[02 - Inputs](./02_inputs/README.md)

[03 - Residents](./03_residents/README.md)

[04 - ASCII Graphics](./04_ascii_graphics/README.md)

[05 - Hans Peter Luhn's Algorithm](./05_luhn/README.md)

[06 - Word Games](./06_words/README.md)

[07 - Coleman Liau Index](./07_coleman_liau_index/README.md)

[08 - Shift Cipher](./08_shift_cipher/README.md)

[09 - Sockets](./09_sockets/README.md)

[10 - Linux](./10_Linux_System_Programming/REAMDE.md)

[11 - Cashier](./11_cashier/README.md)

[12 - Substitution](./12_substitution/README.md)

[13 - Recursion](./13_recursion/README.md)

[14 - Benchmark Sorts](./14_benchmark_sorts/README.md)

[15 - Fast Food](./15_fast_food/README.md)
