# C Portfolio

This repository contains sample programs that I have created/learnt during my
learning. Some of the famous tools being used are:

<table>
<tr>
<td><img
    src="https://i.ibb.co/2FKJjQK/C.png"
    alt="C programming language logo"
    width="64"
/></td>
<td><img
    src="https://i.ibb.co/kKLfPgV/Cmake.png"
    alt="CMake logo"
    width="128"
/></td>
<td><img
    src="https://i.ibb.co/tPJdj5f/Docker.png"
    alt="Docker logo"
    width="92"
/></td>
<td><img
    src="https://i.ibb.co/72NmSxn/doxygen.png"
    alt="Doxygen logo"
    width="261"
/></td>
<td><img
    src="https://i.ibb.co/DwRCqyZ/Vscode.png"
    alt="VSCode logo"
    width="64"
/></td>
<td><img
    src="https://i.ibb.co/7gK3Jbc/Llvm.png"
    alt="LLVM logo"
    width="128"
/></td>
<td><img
    src="https://i.ibb.co/0fqw3zb/Gnu.png"
    alt="GNU logo"
    width="64"
/></td>
</tr>
</table>

## License

This work is covered under MIT License, unless otherwise stated.

## Environment

The development environment has/need the following:

- Ubuntu 20.04
- Intel Core i5 and higher
- Minimum of 8GB memory
- VSCode
- Docker with remote development extensions

## Dependencies

Programs mentioned here so far depend on CMake as a build system. Libraries
needed to be installed are as follows:

### Unit Testing

#### Check

Check is the framework of choice to create test cases. Find more information
about it in [**libcheck**](https://libcheck.github.io/check/#).

> Ubuntu: sudo apt-get install check

#### GTest

Google Test is a unit testing library for the C++ programming language, based on
the xUnit architecture. Find out more
[here](https://google.github.io/googletest/).

### PlantUML

```bash
sudo apt install openjdk-8-jdk
sudo apt install graphviz
```

### Documentation

Download [doxygen](https://www.doxygen.nl/download.html) tool to document your
work.

```bash
sudo apt-get install flex
sudo apt-get install bison
git clone https://github.com/doxygen/doxygen.git
cd doxygen
mkdir build
cd build
cmake -G "Unix Makefiles" ..
make
make install
```

## Build from sources

The following is a sample of how to trigger local builds using `gcc` compiler.

```bash
git clone https://minamaged113@bitbucket.org/minamaged113/c_basics.git
cd c_basics
BUILD_GENERATOR="Ninja"
SOURCE_DIR=$PWD
BUILD_DIR=$SOURCE_DIR/build
C=$(which gcc)
CXX=$(which g++)
BUILD_TYPE="Debug"
DEBUG_LEVEL=10
cmake \
    --no-warn-unused-cli \
    -DCMAKE_EXPORT_COMPILE_COMMANDS:BOOL=TRUE \
    -DCMAKE_BUILD_TYPE:STRING=$BUILD_TYPE \
    -DCMAKE_C_COMPILER:FILEPATH=$C \
    -DCMAKE_CXX_COMPILER:FILEPATH=$CXX \
    -DLOG_LEVEL=$DEBUG_LEVEL \
    -H$SOURCE_DIR \
    -B$BUILD_DIR \
    -G $BUILD_GENERATOR
cmake \
    --build \
    $BUILD_DIR \
    --config $BUILD_TYPE \
    --target all \
    -j 6 --
```

Then you can create the documentation output by calling the following:

```bash
cd docs
doxygen Doxyfile
```

The generated html can be found in `./html/index.html`.

## References

- For all LLVM releases: https://releases.llvm.org/download.html
- Clang 10 Docs: https://releases.llvm.org/10.0.0/tools/clang/docs/index.html
- C Warning Options: https://gcc.gnu.org/onlinedocs/gcc/Warning-Options.html
- Compile flags: https://stackoverflow.com/a/12247461/6081035
- Memory layout for C programs: https://www.geeksforgeeks.org/memory-layout-of-c-program/
