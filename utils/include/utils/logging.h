/**
 * @file logging.h
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief logging functions declarations.
 * @version 0.1
 * @date 2023-03-14
 *
 * @copyright Copyright (c) Mina Ghobrial 2023 under MIT LICENSE
 *
 */
#if !defined(UTILS_LOGGING_H)
#define UTILS_LOGGING_H

#if defined(LOG_LEVEL)
#if (LOG_LEVEL == 10)
#define DEBUG 1
#define INFO 1
#define WARN 1
#define ERROR 1
#define FATAL 1
#endif

#if (LOG_LEVEL == 20)
#define DEBUG 0
#define INFO 1
#define WARN 1
#define ERROR 1
#define FATAL 1
#endif

#if (LOG_LEVEL == 30)
#define DEBUG 0
#define INFO 0
#define WARN 1
#define ERROR 1
#define FATAL 1
#endif

#if (LOG_LEVEL == 40)
#define DEBUG 0
#define INFO 0
#define WARN 0
#define ERROR 1
#define FATAL 1
#endif

#if (LOG_LEVEL == 50)
#define DEBUG 0
#define INFO 0
#define WARN 0
#define ERROR 0
#define FATAL 1
#endif
#endif

#include <bsd/string.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

void LogDebug(const char *fmt, ...);
void LogInfo(const char *fmt, ...);
void LogWarn(const char *fmt, ...);
void LogError(const char *fmt, ...);
void LogFatal(const char *fmt, ...);

#endif // UTILS_LOGGING_H
