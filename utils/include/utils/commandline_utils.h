#if !defined(UTILS_COMMANDLINE_UTILS_H)
#define UTILS_COMMANDLINE_UTILS_H

#include <stdbool.h>
#include <stdio.h>

/**
 * @brief Print the number of input arguments to the commandline, and the
 *  arguments each on a new line. Calling the program as follows:
 *      ./prog arg1 arg2
 *  produces an output that looks like the following:
 *      Given arg coung: 3
 *      Argument 0: ./prog
 *      Argument 1: arg1
 *      Argument 2: arg2
 * 
 * @param argc Integer that indicates the number of commandline args
 * @param argv Array of strings, where each entry corresponds to an input
 *  commndline argument
 */
void PrintArgs(int argc, const char *argv[]);

/**
 * @brief Check if the number of input arguments is a specific number.
 *  NOTE: This function ignore the program name from the argument count
 * 
 *  Example:
 *      If a program has been called as follows: `./prog a1 a2`
 *      NumberOfArgsIs(argc, 2); // would return true
 * 
 * @param input_arguments_count Expected number of arguments after program name
 * @return true If the number of arguments after the program name is equal to
 *          `input_arguments_count`
 * @return false Otherwise
 */
bool NumberOfArgsIs(unsigned int argc, size_t input_arguments_count);

#endif // UTILS_COMMANDLINE_UTILS_H
