/**
 * @file math_utils.h
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief Declare functions for simple math usage
 * @version 0.1
 * @date 2022-09-23
 *
 * @copyright Copyright (c) Mina Ghobrial 2022 under MIT LICENSE
 *
 */
#ifndef UTILS_MATH_UTILS_H
#define UTILS_MATH_UTILS_H

#include <limits.h>  // for __INT_MIN__
#include <stdarg.h>  // for vararg functions
#include <stdbool.h> // to use bools
#include <stdio.h>
#include <stdlib.h> // for strtol

#define BUFFER_SIZE 1024

typedef enum { kBase10 = 10 } StringUtils;

/**
 * @brief Check if a given integer is negative
 *
 * @param value Integer to check if negative
 * @return true Returns true only if negative
 * @return false Returns false if not negative
 */
bool IsNegative(int value);

/**
 * @brief Get number of coins from user
 *
 * @return unsigned int Returns the number of coins that makes up the given
 *  number. This function uses greedy approach.
 */
unsigned int GetInt(void);

/**
 * @brief Add 2 numbers
 *
 * @param augend long representing augend
 * @param addend long representing addend
 * @return long representing sum
 */
long Add(long augend, long addend);

/**
 * @brief Subtract 2 numbers
 *
 * @param minuend long representing Minuend
 * @param subtrahend long representing Subtrahend
 * @return long representing difference
 */
long Sub(long minuend, long subtrahend);

/**
 * @brief Add 2 floats
 *
 * @param augend float representing addend
 * @param addend float representing augend
 * @return float representing sum
 */
double AddFloats(float augend, float addend);

/**
 * @brief Subtract 2 floats
 *
 * @param minuend float representing Minuend
 * @param subtrahend float representing Subtrahend
 * @return float representing difference
 */
double SubFloats(float minuend, float subtrahend);

/**
 * @brief Finds the minimum of a set of given numbers.
 *
 * @param count Number of entered numbers.
 * @param ... Given numbers to compare.
 * @return double Minimum value.
 */
double Min(int count, ...);

/**
 * @brief Finds the maximum of a set of given numbers.
 *
 * @param count Number of entered numbers.
 * @param ... Given numbers to compare.
 * @return double Maximum value.
 */
double Max(int count, ...);

#endif // UTILS_MATH_UTILS_H
