#ifndef UTILS_SOCKET_UTILS_H
#define UTILS_SOCKET_UTILS_H

#include <stdint.h>

#define FREQUENCY (0.01)
#define LOCAL_HOST_IP_ADDR "127.0.0.1"

typedef enum {
    kMaxConnections = 5,
    kPort = 5001,
    kWrtiableDataSize = 1024
} SokcetUtils;

#endif
