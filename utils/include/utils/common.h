/**
 * @file common.h
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief Common defines and MACROs.
 * @version 0.1
 * @date 2023-03-15
 *
 * @copyright Copyright (c) Mina Ghobrial 2023 under MIT LICENSE
 *
 */
#if !defined(UTILS_COMMON_H)
#define UTILS_COMMON_H

#define EXIT_SUCCESS 0
#define EXIT_FAILURE 1
#define BUFFER_SIZE 1024
#define AND &&
#define OR ||
#define NOT !
#define BAND (&)
#define BOR (|)

#endif // UTILS_COMMON_H
