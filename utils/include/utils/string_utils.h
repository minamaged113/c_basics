/**
 * @file string_utils.h
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief Set of functions that are needed to deal with strings that are not
 *  part of the standard library.
 * @version 0.1
 * @date 2023-01-22
 *
 * @copyright Copyright (c) Mina Ghobrial 2023 under MIT LICENSE
 *
 */

#if !defined(UTILS_STRING_UTILS_H)
#define UTILS_STRING_UTILS_H

#include <stdlib.h>
#include <string.h>

int ReturnOne(void);

#endif // UTILS_STRING_UTILS_H
