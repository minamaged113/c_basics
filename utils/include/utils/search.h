/**
 * @file search.h
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief Search functions declarations.
 * @version 0.1
 * @date 2023-03-15
 *
 * @copyright Copyright (c) Mina Ghobrial 2023 under MIT LICENSE
 *
 */

#if !defined(UTILS_SEARCH_H)
#define UTILS_SEARCH_H

#include "common.h"
#include <stdint.h>

#define NOT_FOUND (-1UL)

/**
 * @brief search an array of characters for a given character
 *
 * @param c character to look for.
 * @param arr array of characters to look through.
 * @param size size of the given array.
 * @return index of the character inside the given array if found. Otherwise
 *  returns -1
 */
uint64_t CharLinearSearch(char c, const char *arr, uint64_t size);

int64_t CharBinarySearch(char c, const char *arr, int64_t start, int64_t end);

#endif // UTILS_SEARCH_H
