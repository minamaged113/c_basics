/**
 * @file sort.h
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief Sort function declarations.
 * @version 0.1
 * @date 2023-03-15
 *
 * @copyright Copyright (c) Mina Ghobrial 2023 under MIT LICENSE
 *
 */

#if !defined(UTILS_SORT_H)
#define UTILS_SORT_H

#include "utils/common.h"
#include <stdint.h>
#include <stdlib.h>

void SelectionSort(int32_t *arr, uint64_t size);
void BubbleSort(int32_t *arr, uint64_t size);
void MergeSort(int32_t *arr, uint64_t size);

#endif // UTILS_SORT_H
