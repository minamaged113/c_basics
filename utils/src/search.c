/**
 * @file search.c
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief Search functions definitions.
 * @version 0.1
 * @date 2023-03-15
 *
 * @copyright Copyright (c) Mina Ghobrial 2023 under MIT LICENSE
 *
 */
#include "utils/search.h"

uint64_t CharLinearSearch(const char c, const char *arr, uint64_t size) {
    uint64_t n_found_at = NOT_FOUND;

    for (uint64_t i = 0; i < size; i++) {
        if (*(arr + i) == c) {
            n_found_at = i;
        }
    }

    return n_found_at;
}

int64_t CharBinarySearch(const char c, const char *arr, int64_t start,
                         int64_t end) {
    int64_t found_at = -1;
    int64_t n_mid = -1;

    if (end - start <= 0) {
        return found_at;
    }

    n_mid = (start + end) / 2;

    if (arr[n_mid] == c) {
        found_at = n_mid;
    } else if (arr[n_mid] > c) {
        found_at = CharBinarySearch(c, arr, start, n_mid - 1);
    } else if (arr[n_mid] < c) {
        found_at = CharBinarySearch(c, arr, n_mid + 1, end);
    }

    return found_at;
}
