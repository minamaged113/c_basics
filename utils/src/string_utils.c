/**
 * @file string_utils.c
 * @author Mina Ghobrial "minamaged113@gmail.com"
 * @brief Set of functions that are needed to deal with strings that are not
 *  part of the standard library.
 * @version 0.1
 * @date 2023-01-22
 *
 * @copyright Copyright (c) Mina Ghobrial 2023 under MIT LICENSE
 *
 */
#include "utils/string_utils.h"

int ReturnOne(void) { return 1; }
