/**
 * @file math_utils.c
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief Implement simple math functionalities.
 * @version 0.1
 * @date 2022-09-23
 *
 * @copyright Copyright (c) Mina Ghobrial 2022 under MIT LICENSE
 *
 */
#include "utils/math_utils.h"

unsigned int GetInt(void) {
    int n_change_owed = INT_MIN; // use lowest value possible (<0)
    char c_input[BUFFER_SIZE] = {0};
    char *ptr = NULL;

    while (IsNegative(n_change_owed)) {
        // Ask the user for input change
        printf("Change owed: ");
        if (fgets(c_input, BUFFER_SIZE, stdin) != NULL) {
            n_change_owed = (int)strtol(c_input, &ptr, kBase10);
        }

        // Read characters from buffer and discard them
        // getline(&buffer, &bufsize, stdin);
    }
    return (unsigned int)n_change_owed;
}

bool IsNegative(int value) {
    if (value < 0) {
        return true;
    }
    return false;
}

long Add(long augend, long addend) { return augend + addend; }

long Sub(long minuend, long subtrahend) { return Add(minuend, -subtrahend); }

double AddFloats(float augend, float addend) {
    return (double)(augend + addend);
}

double SubFloats(float minuend, float subtrahend) {
    return AddFloats(minuend, -subtrahend);
}

double Min(int count, ...) {
    double min_value;
    va_list args;
    va_start(args, count);

    min_value = va_arg(args, double);
    for (int arg = 1; arg < count; arg++) {
        double current_arg = va_arg(args, double);
        min_value = (current_arg < min_value) ? current_arg : min_value;
    }

    va_end(args);
    return min_value;
}

double Max(int count, ...) {
    double max_value;
    va_list args;
    va_start(args, count);

    max_value = va_arg(args, double);
    for (int arg = 1; arg < count; arg++) {
        double current_arg = va_arg(args, double);
        max_value = (max_value < current_arg) ? current_arg : max_value;
    }

    va_end(args);
    return max_value;
}
