/**
 * @file logging.c
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief Logging functions defintions.
 * @version 0.1
 * @date 2023-03-14
 *
 * @copyright Copyright (c) Mina Ghobrial 2023 under MIT LICENSE
 *
 */
#include "utils/logging.h"

#define TIME_BUFFER_SIZE 20

static void Log(const char *log_level, const char *fmt, va_list args) {
    time_t rawtime = 0;
    char datetime[TIME_BUFFER_SIZE];
    struct tm *timeinfo = NULL;

    time(&rawtime);
    timeinfo = localtime(&rawtime);

    strftime(datetime, TIME_BUFFER_SIZE, "%Y:%m:%d:%H:%M:%S", timeinfo);
    datetime[TIME_BUFFER_SIZE - 1] = '\0';

    printf("%s | %5s | ", datetime, log_level);

/**
 * DEVIATION:
 *
 * IGNORE: Calling print functions with non-string literal value could
 * cause `Format string attack`.
 *
 * JUSTIFICATION: library is currently for non-public use only. No need to
 * increase security.
 *
 * REFERENCES: More information can be found:
 * - https://owasp.org/www-community/attacks/Format_string_attack
 *
 */
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wformat-nonliteral"
    (void)vprintf(fmt, args); // NOLINT
#pragma clang diagnostic pop

    printf("\n");
}

void LogDebug(const char *__restrict fmt, ...) {
    if (DEBUG) {
        const char *level_name = "DEBUG";
        va_list args;
        va_start(args, fmt);
        Log(level_name, fmt, args);
        va_end(args);
        return;
    }
}
void LogInfo(const char *fmt, ...) {
    if (INFO) {
        const char *level_name = "INFO";
        va_list args;
        va_start(args, fmt);
        Log(level_name, fmt, args);
        va_end(args);
        return;
    }
}
void LogWarn(const char *fmt, ...) {
    if (WARN) {
        const char *level_name = "WARN";
        va_list args;
        va_start(args, fmt);
        Log(level_name, fmt, args);
        va_end(args);
        return;
    }
}
void LogError(const char *fmt, ...) {
    if (ERROR) {
        const char *level_name = "ERROR";
        va_list args;
        va_start(args, fmt);
        Log(level_name, fmt, args);
        va_end(args);
        return;
    }
}
void LogFatal(const char *fmt, ...) {
    if (FATAL) {
        const char *level_name = "FATAL";
        va_list args;
        va_start(args, fmt);
        Log(level_name, fmt, args);
        va_end(args);
        return;
    }
}
