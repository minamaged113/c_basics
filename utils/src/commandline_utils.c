/**
 * @file commandline_utils.c
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief Contains functions that are useful when creating commandline programs
 * @version 0.1
 * @date 2023-01-15
 *
 * @copyright Copyright (c) Mina Ghobrial 2023 under MIT LICENSE
 *
 */
#include "utils/commandline_utils.h"

void PrintArgs(int argc, const char *argv[]) {

    (void)printf("Given arg count: %d\n", argc);
    for (int i = 0; i < argc; i++) {
        (void)printf("Argument %d: %s\n", i, argv[i]);
    }
}

bool NumberOfArgsIs(unsigned int argc, size_t input_arguments_count) {
    if (argc == (input_arguments_count - 1)) {
        return true;
    }
    return false;
}
