/**
 * @file sort.c
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief Sort function definitions.
 * @version 0.1
 * @date 2023-03-15
 *
 * @copyright Copyright (c) Mina Ghobrial 2023 under MIT LICENSE
 *
 */
#include "utils/sort.h"

void BubbleSort(int32_t *arr, uint64_t size) {
    int64_t n_swap_counter = -1;
    uint64_t n_current_loop_size = size;
    int32_t tmp = 0;

    while (n_swap_counter != 0) {
        n_swap_counter = 0;

        for (uint64_t i = 0; i < n_current_loop_size - 1; i++) {
            if (arr[i] > arr[i + 1]) {
                tmp = arr[i];
                arr[i] = arr[i + 1];
                arr[i + 1] = tmp;
                n_swap_counter++;
            }
        }
        n_current_loop_size--;
    }
}

void SelectionSort(int32_t *arr, uint64_t size) {
    int32_t c_current_lowest = 0;
    int32_t tmp = 0;

    for (uint64_t pass = 0; pass < size; pass++) {
        c_current_lowest = arr[pass];
        for (uint64_t entry = pass; entry < size; entry++) {
            if (arr[entry] < c_current_lowest) {
                c_current_lowest = arr[entry];
                tmp = arr[pass];
                arr[pass] = arr[entry];
                arr[entry] = tmp;
            }
        }
    }
}

static void PopulateMergeArray(const int32_t *src_arr, int32_t *dst_arr,
                               uint64_t size) {
    for (uint64_t i = 0; i < size; i++) {
        dst_arr[i] = src_arr[i];
    }
}

static void Merge(int32_t *dst, const int32_t *left_src, uint64_t l_size,
                  const int32_t *right_src, uint64_t r_size) {
    uint64_t l_index = 0;
    uint64_t r_index = 0;
    uint64_t dst_index = 0;

    while (l_index < l_size AND r_index < r_size) {
        if (left_src[l_index] < right_src[r_index]) {
            dst[dst_index] = left_src[l_index];
            l_index++;
            dst_index++;
        } else {
            dst[dst_index] = right_src[r_index];
            r_index++;
            dst_index++;
        }
    }

    while (l_index < l_size) {
        dst[dst_index] = left_src[l_index];
        l_index++;
        dst_index++;
    }

    while (r_index < r_size) {
        dst[dst_index] = right_src[r_index];
        r_index++;
        dst_index++;
    }
}

void MergeSort(int32_t *arr, uint64_t size) {
    uint64_t half_number_of_elements = 0;
    uint64_t odd = 0;

    int32_t *left_arr;
    int32_t *right_arr;

    if (size < 2) {
        // If given array has less than 2 elements (i.e. 1 or 0), consider it
        // sorted already and return
        return;
    }
    half_number_of_elements = size / 2;
    odd = size % 2;

    // Initialize left sorted array
    left_arr = (int32_t *)malloc(sizeof(int32_t) * half_number_of_elements);
    PopulateMergeArray(arr, left_arr, half_number_of_elements);

    // Initialize right sorted array
    right_arr =
        (int32_t *)malloc(sizeof(int32_t) * (half_number_of_elements + odd));
    PopulateMergeArray(&arr[half_number_of_elements], right_arr,
                       (half_number_of_elements + odd));

    MergeSort(left_arr, half_number_of_elements);
    MergeSort(right_arr, (half_number_of_elements + odd));
    Merge(arr, left_arr, half_number_of_elements, right_arr,
          (half_number_of_elements + odd));

    free(left_arr);
    free(right_arr);
}
