/**
 * @file test_math_utils.hpp
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief Tests functionality of utils/math_utils.
 * @version 0.1
 * @date 2023-03-15
 *
 * @copyright Copyright (c) Mina Ghobrial 2023 under MIT LICENSE
 *
 */
#if !defined(TEST_UTILS_MATH_UTILS_HPP)
#define TEST_UTILS_MATH_UTILS_HPP

#include <gtest/gtest.h>

// prevent C++ name mangling to allow linking to C code
// https://www.geeksforgeeks.org/extern-c-in-c/
#ifdef __cplusplus
extern "C" {
#endif
#include "utils/utils.h"
#ifdef __cplusplus
}
#endif

TEST(math_utils, isNegative_returns_true_if_negative) {
    EXPECT_TRUE(IsNegative(-1));
    EXPECT_TRUE(IsNegative(-21));
}

TEST(math_utils, isNegative_returns_false_if_positive) {
    EXPECT_FALSE(IsNegative(11));
    EXPECT_FALSE(IsNegative(93));
}

TEST(math_utils, test_add_ints) {
    EXPECT_EQ(20, Add(1, 19));
    EXPECT_EQ(3, Add(-2, 5));
    EXPECT_EQ(-4, Add(2, -6));
    EXPECT_EQ(-8, Add(-2, -6));
}

TEST(math_utils, test_sub_ints) {
    EXPECT_EQ(-18, Sub(1, 19));
    EXPECT_EQ(-7, Sub(-2, 5));
    EXPECT_EQ(8, Sub(2, -6));
    EXPECT_EQ(4, Sub(-2, -6));
}

TEST(math_utils, test_min_fn) {
    double epsilon = 0.00001;
    EXPECT_NEAR(2.3, Min(4, 5.0, 13.1, 2.3, 4.9), epsilon);
    EXPECT_NEAR(23.0, Min(3, 50.0, 23.0, 73.0), epsilon);
}

TEST(math_utils, test_max_fn) {
    double epsilon = 0.00001;
    EXPECT_NEAR(13.1, Max(4, 5.0, 13.1, 2.3, 4.9), epsilon);
    EXPECT_NEAR(73.0, Max(3, 50.0, 23.0, 73.0), epsilon);
}

#endif // TEST_UTILS_MATH_UTILS_HPP
