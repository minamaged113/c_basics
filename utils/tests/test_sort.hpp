/**
 * @file sort.cpp
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief Test functionality of utils/sort
 * @version 0.1
 * @date 2023-03-15
 *
 * @copyright Copyright (c) Mina Ghobrial 2023 under MIT LICENSE
 *
 */

#if !defined(TEST_UTILS_SORT_HPP)
#define TEST_UTILS_SORT_HPP

#include <gtest/gtest.h>

// prevent C++ name mangling to allow linking to C code
// https://www.geeksforgeeks.org/extern-c-in-c/
#ifdef __cplusplus
extern "C" {
#endif
#include "utils/sort.h"
#ifdef __cplusplus
}
#endif

TEST(sorting, SelectionSort_unsorted_list) {
    int64_t n_arr_size = 10;
    int32_t arr[] = {24, 6, 52, 56, 19, 26, 56, 83, 96, 84};
    int32_t sorted_array[] = {6, 19, 24, 26, 52, 56, 56, 83, 84, 96};

    SelectionSort(arr, n_arr_size);

    for (int64_t i = 0; i < n_arr_size; i++) {
        EXPECT_EQ(arr[i], sorted_array[i]);
    }
}

TEST(sorting, SelectionSort_unsorted_list_small) {
    int64_t n_arr_size = 5;
    int32_t arr[] = {7, 3, 1, 20, 4};
    int32_t sorted_arr[] = {1, 3, 4, 7, 20};

    SelectionSort(arr, n_arr_size);

    for (int64_t i = 0; i < n_arr_size; i++) {
        EXPECT_EQ(arr[i], sorted_arr[i]);
    }
}

TEST(sorting, SelectionSort_sorted_list_stays_same) {
    int64_t n_arr_size = 4;
    int32_t arr[] = {1, 2, 3, 4};
    int32_t sorted_array[] = {1, 2, 3, 4};

    SelectionSort(arr, n_arr_size);

    for (int64_t i = 0; i < n_arr_size; i++) {
        EXPECT_EQ(arr[i], sorted_array[i]);
    }
}

TEST(sorting, BubbleSort_unsorted_list) {
    int64_t n_arr_size = 5;
    int32_t arr[] = {7, 3, 1, 20, 4};
    int32_t sorted_arr[] = {1, 3, 4, 7, 20};

    BubbleSort(arr, n_arr_size);

    for (int64_t i = 0; i < n_arr_size; i++) {
        EXPECT_EQ(arr[i], sorted_arr[i]);
    }
}

TEST(sorting, MergeSort_unsorted_list_odd) {
    int64_t n_arr_size = 5;
    int32_t arr[] = {7, 3, 1, 20, 4};
    int32_t sorted_arr[] = {1, 3, 4, 7, 20};

    MergeSort(arr, n_arr_size);

    for (int64_t i = 0; i < n_arr_size; i++) {
        EXPECT_EQ(arr[i], sorted_arr[i]);
    }
}

TEST(sorting, MergeSort_unsorted_list_even) {
    int64_t n_arr_size = 8;
    int32_t arr[] = {2, 4, 1, 6, 8, 5, 3, 7};
    int32_t sorted_arr[] = {1, 2, 3, 4, 5, 6, 7, 8};

    MergeSort(arr, n_arr_size);

    for (int64_t i = 0; i < n_arr_size; i++) {
        EXPECT_EQ(arr[i], sorted_arr[i]);
    }
}

#endif // TEST_UTILS_SORT_HPP
