/**
 * @file test_search.hpp
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief Search functions tests.
 * @version 0.1
 * @date 2023-03-15
 *
 * @copyright Copyright (c) Mina Ghobrial 2023 under MIT LICENSE
 *
 */
#if !defined(TEST_UTILS_SEARCH_HPP)
#define TEST_UTILS_SEARCH_HPP

#include <gtest/gtest.h>

// prevent C++ name mangling to allow linking to C code
// https://www.geeksforgeeks.org/extern-c-in-c/
#ifdef __cplusplus
extern "C" {
#endif
#include "utils/search.h"
#ifdef __cplusplus
}
#endif

TEST(searching, CharLinearSearch) {
    const char *arr = "strawberry";

    const char c_unavailable = 'z';

    const char c_letter_b = 'b';
    int64_t n_index_of_b = 5;

    const char c_letter_r = 'r';
    int64_t n_index_of_r = 8; // index of last r

    EXPECT_EQ(NOT_FOUND, CharLinearSearch(c_unavailable, arr, strlen(arr)));
    EXPECT_EQ(n_index_of_b, CharLinearSearch(c_letter_b, arr, strlen(arr)));
    EXPECT_EQ(n_index_of_r, CharLinearSearch(c_letter_r, arr, strlen(arr)));
}

TEST(searching, CharBinarySearch_even_number_of_entries) {
    int64_t n_arr_size = 4;
    int64_t n_index_of_3 = 2;
    const char c_look_for = 3;
    const char arr[] = {1, 2, 3, 4};

    EXPECT_EQ(n_index_of_3, CharBinarySearch(c_look_for, arr, 0, n_arr_size));
}

TEST(searching, CharBinarySearch_entry_does_not_exist) {
    int64_t n_arr_size = 4;
    int64_t n_index_of_3 = -1;
    const char c_look_for = 3;
    const char arr[] = {10, 20, 30, 40};

    EXPECT_EQ(n_index_of_3, CharBinarySearch(c_look_for, arr, 0, n_arr_size));
}

#endif // TEST_UTILS_SEARCH_HPP
