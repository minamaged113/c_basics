/**
 * @file main.cpp
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief utils library main tests. Includes all other tests.
 * @version 0.1
 * @date 2023-01-22
 *
 * @copyright Copyright (c) Mina Ghobrial 2023 under MIT LICENSE
 *
 */

#include "test_math_utils.hpp"
#include "test_search.hpp"
#include "test_sort.hpp"
#include <gtest/gtest.h>

int main(int argc, char *argv[]) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
