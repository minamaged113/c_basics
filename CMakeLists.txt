set(C_BASICS_CMAKE_GLOBAL_VERSION "3.0.0")
cmake_minimum_required(VERSION ${C_BASICS_CMAKE_GLOBAL_VERSION})

project(c_basics
LANGUAGES C CXX)

set(CMAKE_C_STANDARD 11)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_C_STANDARD_REQUIRED True)
set(CMAKE_CXX_STANDARD_REQUIRED True)

set(CMAKE_BUILD_TYPE "Debug")
set(CMAKE_EXPORT_COMPILE_COMMANDS "ON")
set(C_BASICS_TOOLS_DIR "${CMAKE_SOURCE_DIR}/tools")

include(CTest)
enable_testing()
include(CPack)

set(CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/cmake")
find_package(Check REQUIRED)
find_package(GTest REQUIRED)

if (CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
  # using Clang
  add_compile_options(-W -Weverything -Werror -ggdb3 -Og)

  # "-Wno-unsafe-buffer-usage" has been added to address:
  # https://discourse.llvm.org/t/rfc-c-buffer-hardening/65734/85
  add_compile_options(-Wno-unsafe-buffer-usage)
  add_compile_options(-Wno-documentation-unknown-command)
  add_compile_options(-Wno-padded)
  add_compile_options(-Wno-error=c++98-compat -Wdocumentation)
  elseif (CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
  # using GCC
  add_compile_options(-w  -Wextra  -Wall -ggdb3 -Og)
endif()

add_subdirectory(cmake)
add_subdirectory(utils)

add_subdirectory(01_hello_world)
add_subdirectory(02_inputs)
add_subdirectory(03_residents)
add_subdirectory(04_ascii_graphics)
add_subdirectory(05_luhn)
add_subdirectory(06_words)
add_subdirectory(07_coleman_liau_index)
add_subdirectory(08_shift_cipher)
add_subdirectory(09_sockets)
add_subdirectory(10_Linux_System_Programming)
add_subdirectory(11_cashier)
add_subdirectory(12_substitution)
add_subdirectory(13_recursion)
add_subdirectory(14_benchmark_sorts)
add_subdirectory(15_fast_food)
add_subdirectory(16_pointer_decay)
add_subdirectory(17_looping_print)
