/**
 * @file main.c
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief
 * @version 0.1
 * @date 2023-03-19
 *
 * @copyright Copyright (c) Mina Ghobrial 2023 under MIT LICENSE
 *
 */

#include "fast_food/mainlib.h"

int main(void) {
    Restaurant out[NUMBER_OF_RESTAURANTS];
    uint32_t n_number_of_winners = 0;
    uint32_t winner_score = 0;

    // PrintRestaurants(restaurants, NUMBER_OF_RESTAURANTS);
    Poll(restaurants);
    SortRestautants(restaurants, NUMBER_OF_RESTAURANTS);
    winner_score = restaurants[NUMBER_OF_RESTAURANTS - 1].popularity;
    n_number_of_winners = SearchRestaurants(winner_score, restaurants,
                                            NUMBER_OF_RESTAURANTS, out);
    PrintRestaurants(out, n_number_of_winners);

    return EXIT_SUCCESS;
}
