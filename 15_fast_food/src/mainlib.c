/**
 * @file mainlib.c
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief Fast Food function definitions.
 * @version 0.1
 * @date 2023-03-19
 *
 * @copyright Copyright (c) Mina Ghobrial 2023 under MIT LICENSE
 *
 */

#include "fast_food/mainlib.h"

Restaurant restaurants[] = {{"McDonald's", 0},
                            {"StarBucks", 0},
                            {"Subway", 0},
                            {"Taco Bell", 0},
                            {"Burger King", 0}};

void SortRestautants(Restaurant *r_arr, uint32_t size) {
    uint32_t n_current_lowest = 0;
    Restaurant tmp;

    for (uint64_t pass = 0; pass < size; pass++) {
        n_current_lowest = r_arr[pass].popularity;
        for (uint64_t entry = pass; entry < size; entry++) {
            if (r_arr[entry].popularity < n_current_lowest) {
                n_current_lowest = r_arr[entry].popularity;
                tmp = r_arr[pass];
                r_arr[pass] = r_arr[entry];
                r_arr[entry] = tmp;
            }
        }
    }
}

void PrintRestaurants(Restaurant *r_arr, uint32_t size) {
    for (uint32_t i = 0; i < size; i++) {
        printf("%s\n", r_arr[i].name);
    }
}

uint32_t SearchRestaurants(uint32_t value, Restaurant *r_arr, uint32_t size,
                           Restaurant *r_out) {
    uint32_t r_out_index = 0;
    for (uint32_t i = 0; i < size; i++) {
        if (r_arr[i].popularity == value) {
            r_out[r_out_index] = r_arr[i];
            r_out_index++;
        }
    }
    return r_out_index;
}
void Poll(Restaurant *r_arr) {
    size_t n_bufsize = BUFFER_SIZE;
    char *pc_buffer = (char *)calloc(n_bufsize, sizeof(char));

    printf("Enter 0 instead of a vote to exit.\n");
    while ((strcmp(pc_buffer, "0\n")) != 0) {
        printf("Cast your vote for: McDonald's, StarBucks, Subway, Taco Bell, "
               "Burger King: ");
        getline(&pc_buffer, &n_bufsize, stdin);

        if (strcmp(pc_buffer, "McDonald's\n") == 0) {
            r_arr[0].popularity++;
        } else if (strcmp(pc_buffer, "StarBucks\n") == 0) {
            r_arr[1].popularity++;
        } else if (strcmp(pc_buffer, "Subway\n") == 0) {
            r_arr[2].popularity++;
        } else if (strcmp(pc_buffer, "Taco Bell\n") == 0) {
            r_arr[3].popularity++;
        } else if (strcmp(pc_buffer, "Burger King\n") == 0) {
            r_arr[4].popularity++;
        } else {
            if ((strcmp(pc_buffer, "0\n")) != 0) {
                LogError("%s: Invalid vote casted", pc_buffer);
            }
            continue;
        }
    }

    free(pc_buffer);
}
