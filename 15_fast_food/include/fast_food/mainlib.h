/**
 * @file mainlib.h
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief Fast Food function declarations.
 * @version 0.1
 * @date 2023-03-19
 *
 * @copyright Copyright (c) Mina Ghobrial 2023 under MIT LICENSE
 *
 */

#if !defined(FAST_FOOD_MAINLIB_H)
#define FAST_FOOD_MAINLIB_H

#include "utils/utils.h"
#include <string.h>

#define MAX_RESTAURANT_NAME_SIZE 20
#define NUMBER_OF_RESTAURANTS 5

typedef struct {
    char name[MAX_RESTAURANT_NAME_SIZE];
    uint32_t popularity;
} Restaurant;

extern Restaurant restaurants[NUMBER_OF_RESTAURANTS];

void PrintRestaurants(Restaurant *restaurants, uint32_t size);
void SortRestautants(Restaurant *restaurants, uint32_t size);
uint32_t SearchRestaurants(uint32_t value, Restaurant *r_arr, uint32_t size,
                           Restaurant *r_out);
void Poll(Restaurant *r_arr);

#endif // FAST_FOOD_MAINLIB_H
