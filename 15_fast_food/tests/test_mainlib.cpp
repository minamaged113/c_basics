/**
 * @file test_mainlib.cpp
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief Fast Food function tests.
 * @version 0.1
 * @date 2023-03-19
 *
 * @copyright Copyright (c) Mina Ghobrial 2023 under MIT LICENSE
 *
 */

#include <gtest/gtest.h>

// prevent C++ name mangling to allow linking to C code
// https://www.geeksforgeeks.org/extern-c-in-c/
#ifdef __cplusplus
extern "C" {
#endif
#include "fast_food/mainlib.h"
#ifdef __cplusplus
}
#endif

TEST(fast_food, DUMMY_TEST) { EXPECT_EQ(1, 1); }

int main(int argc, char *argv[]) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
