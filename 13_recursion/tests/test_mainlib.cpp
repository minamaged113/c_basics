/**
 * @file test_mainlib.c
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief Recursion functions tests.
 * @version 0.1
 * @date 2023-02-25
 *
 * @copyright Copyright (c) Mina Ghobrial 2023 under MIT LICENSE
 *
 */
#include <gtest/gtest.h>

// prevent C++ name mangling to allow linking to C code
// https://www.geeksforgeeks.org/extern-c-in-c/
#ifdef __cplusplus
extern "C" {
#endif
#include "recursion/mainlib.h"
#ifdef __cplusplus
}
#endif

TEST(recursion, Fibonacci_edges) {
    EXPECT_EQ(0, Fibonacci(0));
    EXPECT_EQ(1, Fibonacci(1));
}

TEST(recursion, Fibonacci_trials) {
    EXPECT_EQ(1, Fibonacci(2));
    EXPECT_EQ(13, Fibonacci(7));
}

TEST(recursion, Collatz_conjecture) {
    EXPECT_EQ(0, Collatz(1));
    EXPECT_EQ(1, Collatz(2));
    EXPECT_EQ(7, Collatz(3));
    EXPECT_EQ(2, Collatz(4));
    EXPECT_EQ(5, Collatz(5));
}

int main(int argc, char *argv[]) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
