/**
 * @file mainlib.c
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief Recursion functions defintions.
 * @version 0.1
 * @date 2023-02-25
 *
 * @copyright Copyright (c) Mina Ghobrial 2023 under MIT LICENSE
 *
 */
#include "recursion/mainlib.h"

uint64_t Fibonacci(uint32_t n) {
    if (n == 0 | n == 1) {
        return (uint64_t)n;
    }
    return Fibonacci(n - 1) + Fibonacci(n - 2);
}

uint64_t Collatz(uint32_t n) {
    if (n <= 1) {
        return 0UL;
    }

    if (n % 2 == 0) {
        return 1 + Collatz(n / 2);
    }
    return 1 + Collatz(3 * n + 1);
}
