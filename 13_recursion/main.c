/**
 * @file main.c
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief main recursion functionality
 * @version 0.1
 * @date 2023-02-25
 *
 * @copyright Copyright (c) Mina Ghobrial 2023 under MIT LICENSE
 *
 */
#include "recursion/mainlib.h"

int main(void) { return EXIT_SUCCESS; }
