# Recursion

Demonstrate simple recursion examples.

## Fibonacci

A sequence in which each number is the sum of the two preceding ones.

## Collatz Conjecture

One of the most famous unsolved problems in mathematics. The conjecture asks
whether repeating two simple arithmetic operations will eventually transform
every positive integer into 1.

## References

- https://en.wikipedia.org/wiki/Fibonacci_sequence
- https://en.wikipedia.org/wiki/Collatz_conjecture
