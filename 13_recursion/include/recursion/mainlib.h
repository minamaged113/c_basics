/**
 * @file mainlib.h
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief Recursion functions declarations.
 * @version 0.1
 * @date 2023-02-25
 *
 * @copyright Copyright (c) Mina Ghobrial 2023 under MIT LICENSE
 *
 */

#if !defined(RECURSION_MAINLIB_H)
#define RECURSION_MAINLIB_H

#include "utils/utils.h"

uint64_t Fibonacci(uint32_t n);

uint64_t Collatz(uint32_t n);

#endif // RECURSION_MAINLIB_H
