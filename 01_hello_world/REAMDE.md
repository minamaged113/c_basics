# Hello World

This is a simple `Hello World` application. You can use it to test out that
the development environment is running correctly.

## Sample run

```bash
./program
Hello, World! from C_Basics!
Length of printed string: 29
The length is +1 because of the null terminator
```

## Notes

- Adding `void` cast to functions with unused return values
[explained](https://stackover\flow.com/a/6684438/6081035)
