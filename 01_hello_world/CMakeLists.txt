cmake_minimum_required(VERSION ${C_BASICS_CMAKE_GLOBAL_VERSION})

set(TARGET "01_hello_world")

add_executable(${TARGET} main.c)

target_link_libraries(${TARGET}
PUBLIC ${C_BASICS_UTILS_LIBRARY}
)


target_include_directories(${TARGET}
PUBLIC ${C_BASICS_UTILS_INCLUDES}
)

print_target_properties(${TARGET})
