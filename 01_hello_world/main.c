/**
 * @file main.c
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief Prints `Hello, World!`
 *
 * Constructs a simple program that can be used to test a working environment
 * from compiling to linking.
 * It also demonstrates lightly the return value of a `printf` function and how
 * to cast it to `void`.
 *
 * @version 0.1
 * @date 2022-08-29
 *
 * @copyright Copyright (c) Mina Ghobrial 2022 under MIT LICENSE
 *
 */
#include "utils/utils.h"

/**
 * @brief The main program function
 *
 * @return int
 */
int main(void) {
    int returned_by_printf = printf("Hello, World! from C_Basics!\n");
    (void)printf("Length of printed string: %d\n", returned_by_printf);
    (void)printf("The length is +1 because of the null terminator\n");
    return EXIT_SUCCESS;
}
