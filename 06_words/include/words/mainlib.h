/**
 * @file mainlib.h
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief Objects definitions for computing words scores.
 * @version 0.1
 * @date 2022-08-29
 * 
 * @copyright Copyright (c) Mina Ghobrial 2022
 * 
 */
#ifndef WORDS_MAINLIB_H
#define WORDS_MAINLIB_H

#include <ctype.h>
#include <stdio.h>
#include <string.h>

#define EXIT_FAILURE 1
#define EXIT_SUCCESS 0
#define MAX_LIMIT 50

/**
 * @brief An array of score points for each letter of the alphabet.
 * 
 */
extern const unsigned char kPoints[26];

/**
 * @brief Compute the score of a given word.
 * 
 * @param word A string of characters to compute its score.
 * @return unsigned int Sum of the scores of each letter in the word.
 */
unsigned int ComputeScore(const unsigned char word[]);

#endif // WORDS_MAINLIB_H
