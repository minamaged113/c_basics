/**
 * @file mainlib.c
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief Contains functionality to calculate word scores.
 * @version 0.1
 * @date 2022-08-29
 *
 * @copyright Copyright (c) Mina Ghobrial 2022
 *
 */
#define OR ||
#include "words/mainlib.h"

const unsigned char kPoints[] = {1, 3, 3, 2,  1, 4, 2, 4, 1, 8, 5, 1, 3,
                                 1, 1, 3, 10, 1, 1, 1, 1, 4, 4, 8, 4, 10};

unsigned int ComputeScore(const unsigned char *word) {
    int n_letter = 0;
    unsigned char uc_letter_score = 0;
    unsigned int un_total = 0;

// loop over every character in the word and calculate its score
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wpointer-sign"
    for (size_t i = 0, word_length = strlen((const char *)word);
         i < word_length; i++) {
#pragma clang diagnostic pop
        // get the letter
        n_letter = tolower((int)word[i]);
        // if not alphanumeric, skip iteration
        if (n_letter<'a' OR n_letter> 'z') {
            continue;
        }
        // get the equivalent points from the array
        uc_letter_score = kPoints[n_letter - 'a'];
        // accumulate points
        un_total += uc_letter_score;
    }
    return un_total;
}
