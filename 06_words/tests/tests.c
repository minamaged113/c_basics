#include "words/mainlib.h"
#include <check.h>

START_TEST(handles_letter_cases_correctly) {
    ck_assert_int_eq(ComputeScore((unsigned char *)"LETTERCASE"),
                     ComputeScore((unsigned char *)"lettercase"));
}
END_TEST
START_TEST(handles_punctuation_correctly) {
    ck_assert_int_eq(ComputeScore((unsigned char *)"Punctuation!?!?"),
                     ComputeScore((unsigned char *)"punctuation"));
}
END_TEST
START_TEST(correctly_identifies_Question_and_Question_as_a_tie) {
    ck_assert_int_eq(ComputeScore((unsigned char *)"Question?"),
                     ComputeScore((unsigned char *)"Question!"));
}
END_TEST
START_TEST(correctly_identifies_drawing_and_illustration_as_a_tie) {
    ck_assert_int_eq(ComputeScore((unsigned char *)"drawing"),
                     ComputeScore((unsigned char *)"illustration"));
}
END_TEST
START_TEST(correctly_identifies_hai_as_winner_over_Oh) {
    ck_assert_int_lt(ComputeScore((unsigned char *)"Oh,"),
                     ComputeScore((unsigned char *)"hai!"));
}
END_TEST
START_TEST(correctly_identifies_COMPUTER_as_winner_over_science) {
    ck_assert_int_gt(ComputeScore((unsigned char *)"COMPUTER"),
                     ComputeScore((unsigned char *)"science"));
}
END_TEST
START_TEST(correctly_identifies_Scrabble_as_winner_over_wiNNeR) {
    ck_assert_int_gt(ComputeScore((unsigned char *)"Scrabble"),
                     ComputeScore((unsigned char *)"wiNNeR"));
}
END_TEST
START_TEST(correctly_identifies_pig_as_winner_over_dog) {
    ck_assert_int_gt(ComputeScore((unsigned char *)"pig"),
                     ComputeScore((unsigned char *)"dog"));
}
END_TEST
START_TEST(correctly_identifies_Skating_as_winner_over_figure) {
    ck_assert_int_lt(ComputeScore((unsigned char *)"figure?"),
                     ComputeScore((unsigned char *)"Skating!"));
}
END_TEST

static Suite *WordsTestSuite(void) {
    Suite *suite = NULL;
    TCase *core = NULL;

    suite = suite_create("06_words");

    core = tcase_create("Core");
    tcase_add_test(core, handles_letter_cases_correctly);
    tcase_add_test(core, handles_punctuation_correctly);
    tcase_add_test(core, correctly_identifies_Question_and_Question_as_a_tie);
    tcase_add_test(core,
                   correctly_identifies_drawing_and_illustration_as_a_tie);
    tcase_add_test(core, correctly_identifies_hai_as_winner_over_Oh);
    tcase_add_test(core, correctly_identifies_COMPUTER_as_winner_over_science);
    tcase_add_test(core, correctly_identifies_Scrabble_as_winner_over_wiNNeR);
    tcase_add_test(core, correctly_identifies_pig_as_winner_over_dog);
    tcase_add_test(core, correctly_identifies_Skating_as_winner_over_figure);

    suite_add_tcase(suite, core);
    return (suite);
}

int main(void) {
    int failed = 0;
    Suite *suite = WordsTestSuite();
    SRunner *runner = srunner_create(suite);

    srunner_run_all(runner, CK_NORMAL);
    failed = srunner_ntests_failed(runner);
    srunner_free(runner);

    return (failed == 0 ? EXIT_SUCCESS : EXIT_FAILURE);
}
