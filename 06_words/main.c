/**
 * @file main.c
 * @author Mina Ghobrial (minamaged113@gmail.com)
 * @brief Words game
 * @version 0.1
 * @date 2022-08-29
 *
 * @copyright Copyright (c) Mina Ghobrial 2022
 *
 */
#include "words/mainlib.h"

int main(void) {
    // Create words allocation in memory
    unsigned char word1[MAX_LIMIT];
    unsigned char word2[MAX_LIMIT];
    unsigned int score1 = 0;
    unsigned int score2 = 0;

    // get the first word
    printf("Player 1: ");
    fgets((char *)word1, MAX_LIMIT, stdin);

    // get the second word
    printf("Player 2: ");
    fgets((char *)word2, MAX_LIMIT, stdin);

    // Score both words
    score1 = ComputeScore(word1);
    score2 = ComputeScore(word2);

    // decide on the windder
    if (score1 > score2) {
        printf("Player 1 wins!\n");
    } else if (score1 < score2) {
        printf("Player 2 wins!\n");
    } else {
        printf("Tie!\n");
    }

    return EXIT_SUCCESS;
}
